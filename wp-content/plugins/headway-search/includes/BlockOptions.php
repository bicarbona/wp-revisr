<?php

    class hsearchhBlockOptions extends HeadwayBlockOptionsAPI {

    public $tabs = array(
		'general' => 'General'
	);

	public $inputs = array(
		'general' => array(
			'search-placeholder' => array(
				'name' => 'search-placeholder',
				'label' => 'Input Text Placeholder',
				'type' => 'text',
				'tooltip' => 'The placeholder is text that will be shown in the Search input and immediately removed after you start typing in the search input.',
				'default' => 'Enter search term and hit enter.'
			),

		'search-product'	=> array(
			'name'	 => 'search-product',
			'type'	 => 'checkbox',
			'label'	 => 'Search Product',
			'default' => false,
			'tooltip' => 'Woocomerce Search'
		),

			'show-button' => array(
				'name'    => 'show-button',
				'label'   => 'Show Search Button',
				'type'    => 'checkbox',
				'default' => true,
				'toggle' => array(
					'true' => array(
						'show' => '#input-search-button'
					),
					'false' => array(
						'hide' => '#input-search-button'
					)
				)
			),

			'search-button' => array(
				'name' => 'search-button',
				'label' => 'Button Text',
				'type' => 'text',
				'tooltip' => 'This will update the Search button text.',
				'default' => 'Search'
			)
		)
	);


    }