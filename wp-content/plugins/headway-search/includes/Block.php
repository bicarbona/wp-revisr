<?php

class hsearchhBlock extends HeadwayBlockAPI {

    public $id = 'hsearchh';
    public $name = 'Headway Search';
    public $options_class = 'hsearchhBlockOptions';
    public $description = 'Custom Headway Search Block';
	public $fixed_height = false;

    
//   public static function dynamic_css($block_id, $block) {


// // DEFAULT CSS 

// $css = '
// .block-type-hsearchh input.field {
// 	width: 100%;
// }
// .block-type-hsearchh form.search-button-visible div {
// 	overflow: hidden;
// 	padding-right: 5px
// }

// .block-type-hsearchh form.search-button-visible input.submit {
// min-width: 60px;
// float: right;
// }
// ';

// return $css . "\n";

		
// }



	function enqueue_action($block_id) {

		/* CSS */
	//	wp_enqueue_style('headway-pin-board', plugins_url(basename(dirname(__FILE__))) . '/css/pin-board.css');		

		/* JS */
	//	wp_enqueue_script('headway-pin-board', plugins_url(basename(dirname(__FILE__))) . '/js/pin-board.js', array('jquery'));		

	}
	
	// public static function init_action($block_id, $block) 
    // {

    // }


    // public static function enqueue_action($block_id, $block, $original_block = null)
    // {

    // }


    public static function dynamic_css($block_id, $block, $original_block = null)
    {


$css = '
.block-type-hsearchh input.field {
	width: 100%;
}
.block-type-hsearchh form.search-button-visible div {
	overflow: hidden;
	padding-right: 5px
}

.block-type-hsearchh form.search-button-visible input.submit {
min-width: 60px;
float: right;
}
';
    }


	// function dynamic_js($block_id, $block = false) {
	// 
	// 	if ( !$block )
	// 		$block = HeadwayBlocksData::get_block($block_id);
	// 
	// 	$js = "
	// 	jQuery(document).ready(function() {
	// 		
	// 	});
	// 	";
	// 
	// 	return $js;
	// 
	// }

     public function setup_elements() {
		$this->register_block_element(array(
			'id' => 'search_wrap',
			'name' => 'Search Form',
			'selector' => '.search-form'
		));

		$this->register_block_element(array(
			'id' => 'search_input',
			'name' => 'Search Input',
			'selector' => '.search-form input.field'
		));

		$this->register_block_element(array(
			'id' => 'search_button',
			'name' => 'Search Button',
			'selector' => '.search-form .submit'
		));
}
    public function content($block) {
      	$search_query = get_search_query();

		$button_hidden_class = parent::get_setting( $block, 'show-button', true ) ? 'search-button-visible' : 'search-button-hidden';

		echo '<form method="get" id="searchform-' . $block['id'] . '" class="search-form ' . $button_hidden_class . '" action="' . esc_url(home_url('/')) .  '">' . "\n";
	

	// vyhladavanie v eshope	
	if(parent::get_setting( $block, 'search-product', true )){
		echo '<input type="hidden" name="post_type" value="product" />';
	}
	// end vyhladavanie v eshope

			if ( parent::get_setting( $block, 'show-button', true ) ) {
				echo '<input type="submit" class="submit" name="submit" id="searchsubmit-' . $block['id'] . '" value="' . esc_attr( parent::get_setting( $block, 'search-button', 'Hľadať' ) ) . '" />' . "\n";
			}


			printf('<div><input id="search-' . $block['id'] . '" class="field" type="text" name="s" value="%1$s" placeholder="%2$s" /></div>' . "\n",
				$search_query ? esc_attr($search_query) : '',
				esc_attr(parent::get_setting($block, 'search-placeholder', 'Hľadaný výraz'))
			);



		echo '</form>' . "\n";
		
    }
}