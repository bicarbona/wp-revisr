<?php
/**
 * Plugin Name: Headway Search
 * Plugin URI:  http://wordpress.org/plugins
 * Description: Custom Headway Search Block
 * Version:     0.1.0
 * Author:      bicarbona
 * Author URI:  
 * License:     GPLv2+
 * Text Domain: hsearchh
 * Domain Path: /languages
 */


define('HSEARCHH_BLOCK_VERSION', '0.1.0');

/**
 * Everything is ran at the after_setup_theme action to insure that all of Headway's classes and functions are loaded.
 **/
add_action('after_setup_theme', 'hsearchh_register');
function hsearchh_register() {

	if ( !class_exists('Headway') )
		return;

	require_once 'includes/Block.php';
	require_once 'includes/BlockOptions.php';
	//require_once 'design-editor-settings.php';

	return headway_register_block('hsearchhBlock', plugins_url(false, __FILE__));
}

/**
 * Prevent 404ing from breaking Infinite Scrolling
 **/
add_action('status_header', 'hsearchh_prevent_404');
function hsearchh_prevent_404($status) {

	if ( strpos($status, '404') && get_query_var('paged') && headway_get('pb') )
		return 'HTTP/1.1 200 OK';

	return $status;
}

/**
 * Prevent WordPress redirect from messing up featured board pagination
 */
add_filter('redirect_canonical', 'hsearchh_redirect');
function hsearchh_redirect($redirect_url) {

	if ( headway_get('pb') )
		return false;

	return $redirect_url;
}