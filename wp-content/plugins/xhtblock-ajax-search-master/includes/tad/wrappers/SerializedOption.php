<?php
namespace tad\wrappers;

use tad\utils\Arr;
use tad\interfaces\FunctionsAdapter;
use tad\adapters\Functions;

class SerializedOption
{
    protected $functions;
    protected $val;
    protected $_data;

    public function __construct($optionName, $isSiteOption = false, \tad\interfaces\FunctionsAdapter $functions = null)
    {
        if (!is_string($optionName)) {
            throw new \InvalidArgumentException("Option name must be a string", 1);
        }
        if (!is_bool($isSiteOption)) {
            throw new \InvalidArgumentException("Is site option must be a boolean value", 2);
        }
        if (is_null($functions)) {
            $functions = new \tad\adapters\Functions();
        }
        $this->functions = $functions;
        $suffix = '';
        if ($isSiteOption) {
            $suffix = '_site';
        }
        $funcName = sprintf('get%s_option', $suffix);
        // defaults to an empty array
        $this->val = $this->functions->$funcName($optionName, array());
        $buf = @unserialize($this->val);
        if (false !== $buf or $buf === 'b:0') {
            $this->val = $buf;
        }
        $this->loadPropertiesFrom($this->val);
    }
    public static function on($optionName, $isSiteOption = false, \tad\interfaces\FunctionsAdapter $functions = null)
    {
        return new self($optionName, $isSiteOption, $functions);
    }
    protected function loadPropertiesFrom($arr)
    {
        $this->_data = Arr::camelBackKeys($this->val);
    }
    public function __get($key)
    {
        if ($key == 'val') {
            return $this->val;
        }
        if (!Arr::isAssoc($this->_data)) {
            return null;
        }
        if (array_key_exists($key, $this->_data)) {
            return $this->_data[$key];
        }
        return null;
    }
}
