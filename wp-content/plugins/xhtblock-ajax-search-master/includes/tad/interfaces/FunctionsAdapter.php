<?php
namespace tad\interfaces;

interface FunctionsAdapter
{
    public function __call($function, $arguments);
}
