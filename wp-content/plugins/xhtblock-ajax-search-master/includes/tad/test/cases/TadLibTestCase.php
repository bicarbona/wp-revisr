<?php
namespace tad\test\cases;

class TadLibTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * Returns a mock of the functions adapter.
     *
     * @param  array  $methods An array containing the methods to mock.
     *
     * @return object          The mocked interface.
     */
    protected function getMockFunctions($methods = array())
    {
        return $this->getMock(
            '\tad\interfaces\FunctionsAdapter',
            array_merge(array('__call'), $methods)
        );
    }
}
