<?php

namespace spec\tad\utils;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use tad\utils\JsObject;

class JsObjectSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('tad\utils\JsObject');
    }
    function it_should_throw_an_error_when_not_passed_an_array()
    {
        $this->shouldThrow('\InvalidArgumentException')->during('setIn', array(23));
    }
    function it_should_return_json_like_string_value_pairs()
    {
        $in = array('some'=>'value');
        $out = '{"some":"value"}';
        $this->setIn($in)->getOut()->shouldReturn($out);
    }
    function it_should_return_quotes_free_version_of_function_value()
    {
        $in = array('callback' => 'function(arg1, arg2, arg3) {console.log "some value";}');
        $out = '{"callback":function(arg1, arg2, arg3) {console.log "some value";}}';
        $this->setIn($in)->getOut()->shouldReturn($out);
    }
    function it_should_return_valid_object_when_passed_two_functions_array()
    {
        $in = array('callback1' => 'function(arg){console.log("hello there");}', 'callback2' => 'function(arg){if(true){console.log("hello there");}}');
        $out = '{"callback1":function(arg){console.log("hello there");},"callback2":function(arg){if(true){console.log("hello there");}}}';
        $this->setIn($in)->getOut()->shouldReturn($out);
    }
    function it_should_properly_print_an_object_with_values_and_a_function()
    {
        $in = array('value1' => 4, 'value2' => 'hello there', 'callback' => 'function(arg){console.log("hello there");}');
        $out = '{"value1":4,"value2":"hello there","callback":function(arg){console.log("hello there");}}';
        $this->setIn($in)->getOut()->shouldReturn($out);
    }
    function it_should_properly_print_an_object_with_a_lot_of_nested_functions()
    {
        $in = array('value' => 23,'callback' => 'function(arg){if(arg){if(arg > 9){if(arg < 23){console.log("hello there");}}}}');
        $out = '{"value":23,"callback":function(arg){if(arg){if(arg > 9){if(arg < 23){console.log("hello there");}}}}}';
        $this->setIn($in)->getOut()->shouldReturn($out);
    }
    function it_should_properly_print_an_object_with_nested_anonymous_functions()
    {
        $in = array('callback' => 'function(arg){if(arg > 10){window.someObj = {callback: function(){console.log("hello there");}}} console.log("object created");}', 'some' => 'value');
        $out = '{"callback":function(arg){if(arg > 10){window.someObj = {callback: function(){console.log("hello there");}}} console.log("object created");},"some":"value"}';
        $this->setIn($in)->getOut()->shouldReturn($out);
    }
    function it_should_echo_proper_script_tag()
    {
        $in = array('callback' => 'function(arg){if(arg > 10){window.someObj = {callback: function(){console.log("hello there");}}} console.log("object created");}', 'some' => 'value');
        $out = "<script type=\"text/javascript\">\n\t//<![CDATA[\n\t\tvar some = {\"callback\":function(arg){if(arg > 10){window.someObj = {callback: function(){console.log(\"hello there\");}}} console.log(\"object created\");},\"some\":\"value\"};\n\t//]]\n</script>";
        $this->setIn($in)->printOut('some',false)->shouldBe($out);
    }
    // field tests
    function it_should_print_proper_output_001()
    {
        $in = array('loadToSelector' => '.block-type-content', 'loadFromSelector' => '.block-type-content .block-content', 'ajaxAnchorClicked' => 'function(evt){console.log("hello there");}');
        $out = '{"loadToSelector":".block-type-content","loadFromSelector":".block-type-content .block-content","ajaxAnchorClicked":function(evt){console.log("hello there");}}';
        $this->setIn($in)->getOut()->shouldReturn($out);
    }
    function it_should_remove_php_chars_escaped_more_than_once()
    {
        $in = array('callback' => 'function(arg) {console.log(\"hello there\");}');
        $out = '{"callback":function(arg) {console.log("hello there");}}';
        $this->setIn($in)->getOut()->shouldReturn($out);
    }
}
