#theAverageDev libraries for WordPress
A set of libraries to make WordPress theme and plugin development easier.

## Description
The libraries are born out of my need to rely on utilitity functions and classes, mainly classes, to be able to develop themes and plugins in a consistently and reliable object-oriented manner.

## Manual installation
The libraries are meant to be used as a 'must-use' plugin.  
1. Upload the entire `/theaveragedeve-libraries` directory to the `/mu-plugins/` directory in your WordPress content folder.

## Content

### Class loader
The plugin packs [jwage's Spl Class Loader](https://gist.github.com/jwage/221634) to get rid of all those inclusione files. The class is included as is and hence the same instructions apply with the only relevant difference in the namespacing

    // Example which loads classes for the Doctrine Common package in the
    // Doctrine\Common namespace.
    $classLoader = new \jwage\SplClassLoader('Doctrine\Common', '/path/to/doctrine');
    $classLoader->register(); 

### Adapters
The library included two adapter classes meant to abstract theme and plugins from the environment and allow their development and testing in isolation.  
The <code>Functions</code> class is a simple wrapper aroung globally defined functions that will make test-driven development of WordPress plugins and themes possible. Below an example of testable sample code

    class SomeClass
    {
        protected $functions;

        public function __construct(\tad\interfaces\FunctionsAdapter $functions = null)
        {
            if(is_null($functions)) {
                $functions = new \tad\adapters\Functions;
            }
            $this->functions = $functions;

            // use the functions adapter
            $this->functions->add_action('someAction', array($this, 'someMethod'));
        }

        public function someMethod($someVar)
        {
            ...
        }

        ...
    }

The <code>Globals</code> adapter works the same way wrapping the superglobals to allow mocking and setting expectations on them. An example of code using those

    class SomeClass
    {
        protected $globals;

        public function __construct(\tad\interfaces\GlobalsAdapter $globals = null)
        {
            if(is_null($globals)) {
                $globals = new \tad\adapters\Globals;
            }
            $this->globals = $globals;
        }
        
        // access the 'foo' var in the '$_SERVER' superglobal
        $this->globals->server('foo');
    }

Please note that in both cases the injected dependency is not the adapter class itself but the interface the class implements to allow writing tests like

    class SomeTest extends \PHPUnit_Framework_TestCase
    {
        public function testWillCallAddActionAtConstructTime()
        {
            $sutStub = $this->getMockBuilder('SomeClass')->disableOriginalConstructor()->getMock();
            $mockFunctions = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'add_action')); 
            $mockFunctions->expects($this->once())->method('add_action')->with('someAction', array($sutStub, 'someMethod'));
            $sutStub->__construct($mockFunctions);
        }
    }

### Theme customizer controls and wrappers

#### ThemeCustomizeSection
Allows objec oriented easy access to theme customization sections and controls.

#### Theme Support
Object-oriented wrapping of the theme support functions.

#### Option
While WordPress sports [its Options API](https://codex.wordpress.org/Options_API) to get and set site options I felt the need for something a little more object-oriented to work with and to be able, of course, to mock in tests.  
I've put together a class that wraps the Options API into an object to allow calls like

    // read the option from the database
    $option = new \tad\wrappers\Option('someOption');
    $theWholeOptionArray = $option->val; // ['someKey' => 'someValue']
    $anOptiontey = $option->someKey; // 'someValue'

The class will load the options at <code>__construct</code> time and will save them back to the database at <code>__destruct</code> time to allow for faster a little more efficient reading and writing.  
The class will expose the original value in the <code>val</code> property and will then expose key/value pairs for array options as properties, so given a property like

    [
    'some key' => 'some value',
    'some-other-key' => 'some other value',
    'some_fancy_key' => 'some more value'
    ]

The class will expose

    $option->val            // the array with camelBack keys
    $option->loadedValue    // the original array with different format keys
    $option->someKey;       // 'some value'
    $option->someOtherKey   // 'some other value'
    $option->someFancyKey   // 'some more value'

The class uses the [<code>Arr</code>](https://github.com/lucatume/tad-libs-for-wordpress/blob/master/includes/tad/utils/Arr.php) and [<code>Str</code>](https://github.com/lucatume/tad-libs-for-wordpress/blob/master/includes/tad/utils/Str.php) utility classes to make my life easier.
An option in the <code>__construct</code> metod allows accessing the properties using the underscore notation

    $option = new \tad\wrappers\Option('someOption', $isSiteOption = false, $underscoreNames = true);
    $option->some_key;
    $option->some_other_key;
    $option->some_fancy_key;

#### headway\BlockSettings
The trivial class allows accessing a block settings in a non static way to allow mocking and TDD.

    $settings = new \tad\wrappers\headway\BlockSettings($thisBlock);
    $someSettingValue = $settings->get('some-setting', '300px');

#### Multi-image control
A theme customizer control meant to allow a user to select or upload multiple images. 

### Utility classes

#### Str
A class to manipulate strings and convert them among different formats.

#### Script
A class that makes scripts and styles enqueueing and registration easier.

#### Exception
Environment aware exceptions.