<?php
/**
 * Plugin Name: theAverageDev libraries
 * Plugin URI:  https://github.com/lucatume/tad-libs-for-wordpress
 * Description: A set of libraries to make WordPress theme and plugin development easier
 * Version:     0.1.0
 * Author:      theAverageDev (Luca Tumedei)
 * Author URI:  http://theAverageDev.com
 * License:     GPLv2+
 * Text Domain: tadlibs
 * Domain Path: /languages
 */

/**
 * Copyright (c) 2014 theAverageDev (Luca Tumedei) (email : luca@theaveragedev.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2 or, at
 * your discretion, any later version, as published by the Free
 * Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * Built using grunt-wp-plugin
 * Copyright (c) 2013 10up, LLC
 * https://github.com/10up/grunt-wp-plugin
 * template modified by theAverageDev (Luca Tumedei) to use classes and
 * autoloading
 */

// Useful global constants
$assetsUrl = plugins_url('assets', __FILE__);
$path = WPMU_PLUGIN_DIR . '/tad-libs-for-wordpress';
define('TADLIBS_ASSETS_URL', $assetsUrl);
define('TADLIBS_PATH', $path);
// Include the autolaoder class if the class is not 
// still defined
if (!class_exists('\jwage\SplClassLoader')) {
    $path = '/includes/jwage/SplClassLoader.php';
    require_once(dirname(__FILE__) . $path);
}
// Add all the folders in the includes folder to autoloading
foreach (glob(dirname(__FILE__) . '/includes/*', GLOB_ONLYDIR) as $folderPath) {
    $folderName = basename($folderPath);
    $classLoader = new \jwage\SplClassLoader($folderName, dirname(__FILE__) . '/includes');
    $classLoader->register();
}

// Register the scripts for later use
add_action( 'wp_enqueue_scripts', 'tad_queueScripts');
if (!function_exists('tad_queueScripts')) {
    function tad_queueScripts()
    {
        $srcRoot = TADLIBS_ASSETS_URL . '/js';
        wp_register_script( 'jquery-address', $srcRoot . '/vendor/jQuery/jquery.address.js', array('jquery'));
        wp_register_script( 'jquery-urlInternal', $srcRoot . '/vendor/jQuery/jquery.urlInternal.min.js', array('jquery'));
        wp_register_script( 'modernizr', $srcRoot . '/vendor/Modernizr/modernizr.min.js');
        wp_register_script( 'spin', $srcRoot . '/vendor/spin.js/spin.min.js');
        wp_register_script( 'velocity', $srcRoot . '/vendor/velocity.js/velocity.min.js');
        wp_register_script( 'jquery-ajaxify', \tad\utils\Script::suffix($srcRoot . '/jQuery-ajaxify.js'), array('jquery', 'modernizr', 'jquery-urlInternal'));
        wp_register_script( 'jquery-ajaxifySubmit', \tad\utils\Script::suffix($srcRoot . '/jQuery-ajaxifySubmit.js'), array('jquery', 'modernizr'));
        wp_register_script( 'breakpoints', \tad\utils\Script::suffix($srcRoot . '/breakpoints.js'), array('jquery', 'modernizr'));
        wp_register_script( 'tad_helpers', \tad\utils\Script::suffix($srcRoot . '/tad_helpers.js'));
    }
}

// Utility function to included files that do not contain PSR
// compatbile classes
function tadlibs_include($relativePath)
{
    if (!is_string($relativePath)) {
        throw new BadArgumentException("Path must be a string", 1);
    }
    $fullPath = dirname(__FILE__) . '/includes/' . $relativePath . '.php';
    if (!file_exists($fullPath)) {
        throw new BadArgumentException("File $fullPath does not exist", 2);
    }
    require_once $fullPath;
}
