/*! theAverageDeve libraries - v0.1.0
 * https://github.com/lucatume/tad-libs-for-wordpress
 * Copyright (c) 2014; * Licensed GPLv2+ */
(function(document, window, undefined) {

    /**
     * Returns a string wrapping its content on a word basis.
     *
     * @param  {int} wordsPerLine How many words to wrap, defaults to 8
     * @param  {string} tag          The HTML element to wrap the words with, defaults to 'span'
     * @param  {string} _class       The wrapper class(es), defaults to 'line'
     * @param  {int} maxChars        Optional max numbers of chars in total
     * @param  {string} ellipsis     Optional ellipsis char to appen when limiting chars, defaults to &hellip;
     *
     * @return {string}
     */
    String.prototype.lineSpliceByWords = function(wordsPerLine, tag, _class, maxChars, ellipsis) {
        var input = this.valueOf();

        if (maxChars !== undefined) {
            input = input.atMostChars(maxChars, ellipsis);
        }

        var words = input.split(' '),
            lines = '',
            step = 1,
            chunk, i, openTag, closeTag;

        wordsPerLine = wordsPerLine || 8;
        ellipsis = ellipsis || '&hellip;';

        tag = tag || 'span';
        _class = _class || 'line';

        openTag = '<' + tag + ' class="' + _class + '">';
        closeTag = '</' + tag + '>';

        words.splice(0, 0, openTag);
        words.push(closeTag);

        chunk = 1 + wordsPerLine;
        i = chunk;

        while (i < words.length) {
            if (words[i] !== closeTag) {
                words.splice(i, 0, closeTag + openTag);
            }
            step++;
            i = step * chunk;
        }
        lines = words.join(' ');
        // trim spaces before and after tags
        lines = lines.replace(/>\s/gi, ">");
        lines = lines.replace(/\s</gi, "<");

        return lines;
    };
    /**
     * Returns a string wrapping its content on a char and then word basis.
     *
     * @param  {int} charsPerLine   How many words to wrap, defaults to 8
     * @param  {string} tag         The HTML element to wrap the words with, defaults to 'span'
     * @param  {string} _class      The wrapper class(es), defaults to 'line'
     * @param  {int} maxChars       Optional max numbers of chars in total
     * @param  {string} ellipsis    Optional ellipsis char to appen when limiting chars, defaults to &hellip;
     *
     * @return {string}
     */
    String.prototype.lineSpliceByChars = function(charsPerLine, tag, _class, maxChars, ellipsis) {
        var input = this.valueOf();

        if (maxChars !== undefined) {
            input = input.atMostChars(maxChars, ellipsis);
        }

        var words = input.split(' '),
            lines = '',
            buffer = '',
            next = '',
            futureBufferLength = 0,
            openTag, closeTag;


        // defaults
        charsPerLine = charsPerLine || 80;
        tag = tag || 'span';
        _class = _class || 'line';

        openTag = '<' + tag + ' class="' + _class + '">';
        closeTag = '</' + tag + '>';


        // reverse the array to consume using pop
        // using `pop` will get the first word
        words.reverse();
        while (words.length > 0) {
            next = words.pop();
            // take next word length and space into account
            futureBufferLength = buffer.length + next.length;
            if (futureBufferLength <= charsPerLine) {
                // if the buffer is shorter than the max number of words
                // add the next word to the buffer
                buffer = buffer + next + ' ';
            } else {
                // add the buffer to the lines
                lines = lines + openTag + buffer.trim() + closeTag;
                // reset the buffer
                buffer = next + ' ';
            }

        }
        if (buffer.length > 0) {
            // add the buffer to the lines
            lines = lines + openTag + buffer.trim() + closeTag;
        }
        return lines;
    };

    /**
     * Returns the string truncated word-wise to a maximum number of chars.
     *
     * @param  int maxChars     The maximum number of chars to return
     * @param  string           Optional ellipsis What to append at the end of the string, defaults to &hellip:
     *
     * @return string
     */
    String.prototype.atMostChars = function(maxChars, ellipsis) {
        var words = this.valueOf().split(' '),
            buffer = '',
            done, next, futureBufferLength;

        ellipsis = ellipsis || '&hellip;';

        words.reverse();
        while (words.length > 0 && !done) {
            next = words.pop();
            futureBufferLength = buffer.trim().length + next.length;
            if (futureBufferLength <= maxChars) {
                buffer += next + ' ';
                next = false;
            } else {
                break;
            }
        }
        buffer = buffer.trim();
        // append the ellipsis char
        if (next) {
            buffer = buffer.trim() + ellipsis.trim();
        }
        return buffer;
    };

})(document, window);