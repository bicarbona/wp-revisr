/*global window, document*/

// based on this breakpoints by Erskine
// xsmall-range: (18.75rem, 24.99rem); /* 300, 399px */
// small-range: (25rem, 31.1875rem); /* 400, 499px */
// medium-range: (31.25rem, 43.6875rem); /* 500px, 699px */
// large-range: (43.75rem, 53.0625rem); /* 700px, 849px */
// xlarge-range: (53.125rem, 62.1875rem); /* 850px, 995px */
// xxlarge-range: (62.25rem, 75rem); /* 996px, 1200px*/

function Size(sizes) {
    if (sizes && sizes.length < 12) {
        throw 'Sizes array should contain 12 sizes from xxSmall to XXLarge';
    }
    if (sizes) {
        this.sizes = sizes;
    } else {
        this.sizes = {
            xSmallLower: 18.75,
            xSmallUpper: 24.99,
            smallLower: 25,
            smallUpper: 31.1875,
            mediumLower: 31.25,
            mediumUpper: 43.6875,
            largeLower: 43.75,
            largeUpper: 53.0625,
            xLargeLower: 53.125,
            xLargeUpper: 62.1875,
            xxLargeLower: 62.25,
            xxLargeUppper: 75
        };
    }
}
Size.prototype.toRem = function() {
    var html = document.getElementsByTagName('html')[0],
        _size = window.getComputedStyle(html, null).getPropertyValue('font-size'),
        remSize = parseFloat(_size),
        windowSize = parseFloat(window.innerWidth);
    return (windowSize / remSize);
};
Size.prototype.atLeastRem = function(value) {
    return this.toRem() >= value;
};
Size.prototype.largerThanRem = function(value) {
    return this.toRem() > value;
};
Size.prototype.smallerThanRem = function(value) {
    return this.toRem() < value;
};
Size.prototype.atMostRem = function(value) {
    return this.toRem() <= value;
};
Size.prototype.inRem = function(min, max) {
    var rem = this.toRem();
    return rem >= min && rem <= max;
};
Size.prototype.notInRem = function(min, max) {
    var rem = this.toRem();
    return rem < min || rem > max;
};
Size.prototype.isXSmall = function() {
    return this.inRem(this.sizes.xSmallLower, this.sizes.xSmallUpper);
};
Size.prototype.isSmall = function() {
    return this.inRem(this.sizes.smallLower, this.sizes.smallUpper);
};
Size.prototype.isMedium = function() {
    return this.inRem(this.sizes.mediumLower, this.sizes.mediumUpper);
};
Size.prototype.isLarge = function() {
    return this.inRem(this.sizes.largeLower, this.sizes.largeUpper);
};
Size.prototype.isXLarge = function() {
    return this.inRem(this.sizes.xLargeLower, this.sizes.xLargeUpper);
};
Size.prototype.isXXLarge = function() {
    return this.inRem(this.sizes.xxLargeLower, this.sizes.xxLargeUppper);
};
Size.prototype.isHuge = function() {
    return this.atLeastRem(this.sizes.xxLargeUppper);
};
Size.prototype.isSmallAndUp = function() {
    return this.atLeastRem(this.sizes.smallLower);
};
Size.prototype.isMediumAndUp = function() {
    return this.atLeastRem(this.sizes.mediumLower);
};
Size.prototype.isLargeAndUp = function() {
    return this.atLeastRem(this.sizes.largeLower);
};
Size.prototype.isXLargeAndUp = function() {
    return this.atLeastRem(this.sizes.xLargeLower);
};
Size.prototype.isXXLargeAndUp = function() {
    return this.atLeastRem(this.sizes.xxLargeLower);
};

window.size = new Size();