/*global jQuery, window, document, console, Modernizr*/
// Utility method for Object.create
if (typeof Object.create !== 'function') {
    Object.create = function(obj) {
        function F() {}
        F.prototype = obj;
        return new F();
    };
}
// Ajaxify jQuery plugin by theAverageDev (Luca Tumedei)
// requires: $.urlInternal and Modernizr
(function($, window, document, undefined) {

    var AjaxifySubmit = {

        _popStateEventCount: 0,

        init: function(options, el) {
            var self = this;

            self.el = el;
            self.$el = $(el);

            // object version of the options 
            self.options = $.extend({}, $.fn.ajaxifySubmit.options, options);

            // get the search placeholder
            self.placeholder = self.$el.find(self.options.getQueryFromSelector).val();

            // bind the submit action to the ajax query
            self.bindSubmitAction();

            // clear on focus
            self.bindClearOnFocus();

            // restore the placeholder on blur
            self.bindOnBlur();

            // live search
            self.maybeBindLiveSearch();

            // maybe manage the popstate
            self.maybeManagePopstate();
        },

        maybeManagePopstate: function() {
            var self = this;
            $(window).on('popstate', function(evt) {
                self._popStateEventCount += 1;
                if ($.browser.webkit && self._popStateEventCount === 1) {
                    return;
                }
                self.loadPage();
            });
        },

        bindClearOnFocus: function() {
            var self = this;
            self.$el.find(self.options.getQueryFromSelector).focus(function() {
                self.callOnFocus();
                if (self.options.clearOnFocus) {
                    var $this = $(this);
                    if (($this).val() === self.placeholder) {
                        $this.val('');
                    }
                }
            });
        },

        bindOnBlur: function() {
            var self = this;
            self.$el.find(self.options.getQueryFromSelector).blur(function() {
                self.callOnBlur();
                if (self.options.restorePlaceholderOnBlur) {
                    $(this).val(self.placeholder);
                }
            });
        },

        bindSubmitAction: function() {
            var self = this;
            self.$el.submit(function(evt) {
                evt.preventDefault();
                self.callSubmitClicked(evt);
                self.maybeUpdateHistory();
                self.loadPage();
            });
        },

        maybeUpdateHistory: function() {
            var self = this;
            if (!self.options.deepLink || !Modernizr.history) {
                return;
            }
            var pageName = self.getQueryUrl();
            window.history.pushState(null, '', pageName);
        },

        maybeBindLiveSearch: function() {
            var self = this,
                input = self.$el.find(self.options.getQueryFromSelector);
            if (!self.options.liveSearch) {
                return;
            }
            input.keyup(function() {
                if (input.val().length < self.options.liveSearchChars) {
                    return;
                }
                if (self.options.liveSearchTimeout) {
                    var timeout;
                    window.clearTimeout(timeout);
                    timeout = window.setTimeout(function() {
                        self.maybeUpdateHistory();
                        self.loadPage();
                    }, self.options.liveSearchTimeout);

                } else {
                    self.maybeUpdateHistory();
                    self.loadPage();
                }
            });
        },

        getQueryUrl: function() {
            var self = this,
                searchInput = self.$el.find(self.options.getQueryFromSelector),
                search = searchInput.val().replace(/ /g, self.options.querySeparator);
            if (search === 'undefined') {
                return '';
            }
            return self.options.queryPathname + search;

        },

        loadPage: function() {
            var self = this,
                url = '',
                $contentArea = $(self.options.loadToSelector);
            self.callBeforeLoad($contentArea);
            url = self.getQueryUrl() + ' ' + self.options.loadFromSelector;
            $contentArea.load(url, function(response, status, xhr) {
                if (status === 'error') {
                    self.callAfterFail($contentArea);
                    return;
                }
                self.callAfterLoad($contentArea);
            });
        },

        callSubmitClicked: function(evt) {
            var self = this;
            if (typeof self.options.submitClicked === 'function') {
                self.options.submitClicked(evt);
            }
        },

        callOnFocus: function(evt) {
            var self = this;
            if (typeof self.options.onFocus === 'function') {
                self.options.onFocus(evt);
            }
        },

        callOnBlur: function(evt) {
            var self = this;
            if (typeof self.options.onBlur === 'function') {
                self.options.onBlur(evt);
            }
        },


        callBeforeLoad: function($contentArea) {
            var self = this;
            if (typeof self.options.beforeLoad === 'function') {
                var content = $contentArea.html();
                self.options.beforeLoad(self.$el, $contentArea, content);
            }
        },

        callAfterLoad: function($contentArea) {
            var self = this;
            if (typeof self.options.afterLoad === 'function') {
                var content = $contentArea.html();
                self.options.afterLoad(self.$el, $contentArea, content);
            }
        },

        callAfterFail: function($contentArea) {
            var self = this;
            if (typeof self.options.afterFail === 'function') {
                var content = $contentArea.html();
                self.options.afterFail(self.$el, $contentArea, content);
            }
        }
    };

    $.fn.ajaxifySubmit = function(options) {
        return this.each(function() {
            var ajaxifySubmit = Object.create(AjaxifySubmit);
            ajaxifySubmit.init(options, this);
        });
    };

    $.fn.ajaxifySubmit.options = {
        getQueryFromSelector: 'input[type=text]',
        querySeparator: '+',
        queryPathname: '/?s=',
        loadFromSelector: '#main-content',
        loadToSelector: '#main',
        excludeSelector: '',
        liveSearch: true,
        liveSearchChars: 3,
        liveSearchTimeout: 1000,
        restorePlaceholderOnBlur: true,
        clearOnFocus: true,
        deepLink: true,
        submitClicked: function(evt) {
            /*no-op*/
        },
        beforeLoad: function($context, $contentArea, oldContent) {
            $contentArea.fadeOut();
        },
        afterLoad: function($context, $contentArea, newContent) {
            $contentArea.fadeIn();
        },
        afterFail: function($context, $contentArea, oldContent) {
            $contentArea().html(oldContent).fadeIn();
        },
        onFocus: function($context, $contentArea, oldContent) {
            /*no-op*/
        },
        onBlur: function($context, $contentArea, oldContent) {
            /*no-op*/
        }
    };

})(jQuery, window, document);