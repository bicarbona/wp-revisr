/*global jQuery, window, document, console, Modernizr*/
// Utility method for Object.create
if (typeof Object.create !== 'function') {
    Object.create = function(obj) {
        function F() {}
        F.prototype = obj;
        return new F();
    };
}
// Ajaxify jQuery plugin by theAverageDev (Luca Tumedei)
// requires: $.urlInternal and Modernizr
(function($, window, document, undefined) {
    var Ajaxify = {
        _popStateEventCount: 0,
        init: function(options, el) {
            var self = this;
            self.el = el;
            self.$el = $(el);
            // object version of the options 
            self.options = $.extend({}, $.fn.ajaxify.options, options);
            self.loadFromSelector = self.options.loadFromSelector;
            self.loadToSelector = self.options.loadToSelector;
            // bind anchor tags to trigger an address change
            this.bindAnchors();
            // to ensure back and forward buttons will work as
            // expected
            this.managePopstate();
        },
        maybePartializeUrls: function() {
            var self = this;
            if (!self.options.base) {
                return;
            }
            self.$el.find('a:urlExternal').not(self.options.excludeSelector).each(function() {
                var anchor = $(this),
                    partialUrl = anchor.attr('href').replace(self.options.base, '') || '/';
                anchor.attr('href', partialUrl);
            });
        },
        bindAnchors: function() {
            // maybe partialize all urls if a base url is given
            this.maybePartializeUrls();
            var self = this,
                internals = self.$el.find('a:urlInternal'),
                externals = self.$el.find('a:urlExternal'),
                excluded = self.$el.find(self.options.excludeSelector),
                legit = internals.not(self.options.excludeSelector);
            // bind an event to the click of any external link
            externals.on('click', function(evt) {
                self.callExternalAnchorClicked(evt);
            });
            // bind an event to the click of internal links excluded from
            // ajaxification
            excluded.click(function(evt) {
                self.callExcludedClicked(evt);
            });
            // load page for links not excluded from ajaxification
            legit.click(function(evt) {
                if (Modernizr.history) {
                    evt.preventDefault();
                    evt.stopPropagation();
                    var pageName = $(this).attr('href');
                    window.history.pushState(null, '', pageName);
                }
                self.callAjaxAnchorClicked(evt);
                self.loadPage();
            });
        },
        loadPage: function(dest) {
            var self = this,
                $contentArea = $(self.loadToSelector),
                url = dest || self.getPageName();
            url += ' ' + self.loadFromSelector;
            self.callBeforeLoad($contentArea);
            $contentArea.load(url, function(response, status, xhr) {
                if (status === 'error') {
                    self.callAfterFail($contentArea);
                    return;
                }
                // partialize new urls pulled in
                self.bindAnchors();
                self.callAfterLoad($contentArea);
            });
        },
        getPageName: function() {
            return window.location.pathname;
        },
        managePopstate: function() {
            var self = this;
            $(window).on('popstate', function(evt) {
                self._popStateEventCount += 1;
                if ($.browser.webkit && self._popStateEventCount === 1) {
                    return;
                }
                self.loadPage();
            });
        },
        callAjaxAnchorClicked: function(evt) {
            var self = this;
            if (self.options.raiseEvents) {
                self.$el.trigger('ajaxAnchorClicked', evt);
            }
            if (self.options.useCallbacks && typeof self.options.ajaxAnchorClicked === 'function') {
                self.options.ajaxAnchorClicked(evt);
            }
        },
        callBeforeLoad: function($contentArea) {
            var self = this,
                content = $contentArea.html();
            if (self.options.raiseEvents) {
                self.$el.trigger('beforeLoad', self.$el, $contentArea, content);
            }
            if (self.options.useCallbacks && typeof self.options.beforeLoad === 'function') {
                self.options.beforeLoad(self.$el, $contentArea, content);
            }
        },
        callAfterLoad: function($contentArea) {
            var self = this,
                content = $contentArea.html();
            if (self.options.raiseEvents) {
                self.$el.trigger('afterLoad', self.$el, $contentArea, content);
            }
            if (self.options.useCallbacks && typeof self.options.afterLoad === 'function') {
                self.options.afterLoad(self.$el, $contentArea, content);
            }
        },
        callAfterFail: function($contentArea) {
            var self = this,
                content = $contentArea.html();
            if (self.options.raiseEvents) {
                self.$el.trigger('afterFail', self.$el, $contentArea, content);
            }
            if (self.options.useCallbacks && typeof self.options.afterFail === 'function') {
                self.options.afterFail(self.$el, $contentArea, content);
            }
        },
        callExternalAnchorClicked: function(evt) {
            var self = this;
            if (self.options.raiseEvents) {
                self.$el.trigger('externalAnchorClicked', evt);
            }
            if (self.options.useCallbacks && typeof self.options.externalAnchorClicked === 'function') {
                self.options.externalAnchorClicked(evt);
            }
        },
        callExcludedClicked: function(evt) {
            var self = this;
            if (self.options.raiseEvents) {
                self.$el.trigger('excludedClicked', evt);
            }
            if (self.options.useCallbacks && typeof self.options.excludedClicked === 'function') {
                self.options.excludedClicked(evt);
            }
        }
    };
    $.fn.ajaxify = function(options) {
        return this.each(function() {
            var ajaxify = Object.create(Ajaxify);
            ajaxify.init(options, this);
        });
    };
    $.fn.ajaxify.options = {
        loadFromSelector: '#main-content',
        loadToSelector: '#main',
        excludeSelector: '',
        useCallbacks: true,
        raiseEvents: true,
        beforeLoad: function($context, $contentArea, oldContent) {
            $contentArea.fadeOut();
        },
        afterLoad: function($context, $contentArea, newContent) {
            $contentArea.fadeIn();
        },
        afterFail: function($context, $contentArea, oldContent) {
            $contentArea.html(oldContent).fadeIn();
        },
        ajaxAnchorClicked: function(evt) {
            /*no-op*/
        },
        externalAnchorClicked: function(evt) {
            /*no-op*/
        },
        excludedClicked: function(evt) {
            /*no-op*/
        }
    };
})(jQuery, window, document);