describe("jQuery.ajaxify", function() {
    var $frag;
    beforeEach(function() {
        // configure the fixtures to be loaded from another folder
        jasmine.getFixtures().fixturesPath = "assets/js/spec/fixtures";
        loadFixtures('frag.html');
        $frag = $("#main");
        // mock all ajax requests
        jasmine.Ajax.install();
        // mock the $.browser.webkit
        $.browser = {
            webkit: 1
        };
    });
    afterEach(function() {
        jasmine.Ajax.uninstall();
    });
    describe("Basic plugin setup", function() {
        it("should be available on the jQuery object", function() {
            expect($.fn.ajaxify).toBeDefined();
        });
        it("should be chainable", function() {
            expect($frag.ajaxify()).toBe($frag);
        });
        it("should return a collection if a collection was passed in", function() {
            expect($frag.find('.sub-menu').ajaxify().length).toBe(2);
        });
        it("offers a defaults object on the ajaxify namespace", function() {
            expect($.fn.ajaxify.options).toBeDefined();
        });
        it("should not load any content when an external click is linked", function() {
            spyOn($, 'isUrlExternal').and.returnValue(true);
            $frag.ajaxify().find('a').first().trigger('click');
            expect($frag.find("#main-content").text()).toBe('lorem ipsum');
        });
    });
    describe("Plugin options", function() {
        it("should call callback functions by default", function() {
            expect($.fn.ajaxify.options.useCallbacks).toBe(true);
        });
        it("should allow plugin users to disable function callbacks", function() {
            var $frag = affix('#menu a.link[href="/some/path"]');
            var beforeLoadSpy = jasmine.createSpy('beforeLoad');
            $frag.ajaxify({
                useCallbacks: false
            }).find('a.link').click();
            expect(beforeLoadSpy).not.toHaveBeenCalled();
        });
        it("should allow a plugin user to specify the afterLoad success callback function", function() {
            // mock the check for the external link
            spyOn($, 'isUrlExternal').and.returnValue(false);
            // create a spy fucntion to be injected in the ajax callback method
            var afterLoadSpy = jasmine.createSpy('afterLoad'),
                content = '<div id="main-content"><p>some new lorem</p></div>';
            // trigger the method that will make the ajax request
            $frag.ajaxify({
                afterLoad: afterLoadSpy
            }).find('a[href="/some/url"]').trigger('click');
            // mock the ajax request
            jasmine.Ajax.requests.mostRecent().response({
                'status': 200,
                'content/type': 'text/html',
                'responseText': content
            });
            // verify the call to the success callback function has been made
            expect(afterLoadSpy).toHaveBeenCalledWith(jasmine.any(Object), jasmine.any(Object), content);
        });
        // it("should allow a plugin user to specify the beforeLoad callback function", function() {
        //     $frag = affix('#main #main-content a.link[href="/some/url"]');
        //     $p = $frag.affix('p.para').text('lorem ipsum');
        //     var beforeLoadSpy = jasmine.createSpy('beforeLoad');
        //     // spyOn($, 'isUrlExternal').and.returnValue(false);
        //     $frag.ajaxify({
        //         beforeLoad: beforeLoadSpy
        //     }).find('a[href="/some/url"]').trigger('click');
        //     expect(beforeLoadSpy).toHaveBeenCalledWith(jasmine.any(Object), jasmine.any(Object), $frag.html());
        // });
        it("should call fadeOut when no beforeLoad method is specified", function() {
            var fadeOutSpy = spyOn($.prototype, 'fadeOut');
            spyOn($, 'isUrlExternal').and.returnValue(false);
            $frag.ajaxify().find('a[href="/some/url"]').trigger('click');
            expect(fadeOutSpy).toHaveBeenCalled();
        });
        it("should call fadeIn when no afterLoad success method is specified", function() {
            var fadeInSpy = spyOn($.prototype, 'fadeIn');
            spyOn($, 'isUrlExternal').and.returnValue(false);
            // apply the plugin to the html fragment
            $frag.ajaxify().find('a[href="/some/url"]').trigger('click');
            // which should trigger the ajax request
            jasmine.Ajax.requests.mostRecent().response({
                'status': 200,
                'content/type': 'text/html',
                'responseText': 'whatever'
            });
            expect(fadeInSpy).toHaveBeenCalled();
        });
        it("should load into #main using the #main-content selector by default", function() {
            var content = '<div id="main-content"><p>some new lorem</p></div>';
            spyOn($, 'isUrlExternal').and.returnValue(false);
            $frag.ajaxify().find('a[href="/some/url"]').trigger('click');
            jasmine.Ajax.requests.mostRecent().response({
                'status': 200,
                'content/type': 'text/html',
                'responseText': content
            });
            expect($frag.find('#main-content').html()).toBe('<p>some new lorem</p>');
        });
        it("should allow plugin users to specify a class selector as the destination and have the content replicated in all the containers", function() {
            // .main is the default load source selector
            var content = '<div class="main"><b>some content</b></div>';
            spyOn($, 'isUrlExternal').and.returnValue(false);
            $frag.ajaxify({
                loadToSelector: 'p.item'
            }).find('a[href="/some/url"]').trigger('click');
            jasmine.Ajax.requests.mostRecent().response({
                'status': 200,
                'content/type': 'text/html',
                'responseText': content
            });
            $frag.find('p.item').each(function() {
                var $this = $(this);
                expect($this.html()).toBe(content);
            })
        });
    });
    describe("Callback function usage", function() {
        it("should allow for elements to be inserted and removed in beforeLoad and afterLoad function callbacks", function() {
            spyOn($, 'isUrlExternal').and.returnValue(false);
            var afterLoad = jasmine.createSpy('someFunctionRemovingTheSpinner');
            $('#wrapper').ajaxify({
                loadToSelector: 'p.item',
                beforeLoad: function() {
                    $('#wrapper').append(sandbox({
                        class: 'spinner'
                    }));
                },
                afterLoad: afterLoad
            }).find('a').first().trigger('click');
            // here the response is not in yet, beforeLoad was called,
            // the spinner should be there
            expect($('#wrapper .spinner').length).toBe(1);
            // send the response
            jasmine.Ajax.requests.mostRecent().response({
                'status': 200,
                'content/type': 'text/html',
                'responseText': 'whatever'
            });
            // spinner should have been removed
            expect(afterLoad).toHaveBeenCalled();
        });
        it("should call the afterFail call back function specified by the user", function() {
            spyOn($, 'isUrlExternal').and.returnValue(false);
            var failSpy = jasmine.createSpy('afterFail');
            $frag.ajaxify({
                loadToSelector: 'p.item',
                afterFail: failSpy
            }).find('a[href="/some/url"]').first().trigger('click');
            jasmine.Ajax.requests.mostRecent().response({
                'status': 404,
                'content/type': 'text/html',
                'responseText': 'will not use this'
            });
            expect(failSpy).toHaveBeenCalled();
        });
        // it("should call the afterFail call back function specified by the user with old content", function() {
        //     spyOn($, 'isUrlExternal').and.returnValue(false);
        //     var failSpy = jasmine.createSpy('afterFail'),
        //         content = $frag.find('#main-content').html();
        //     $frag.ajaxify({
        //         loadToSelector: '#some-selector',
        //         afterFail: failSpy
        //     }).find('a[href="/some/url"]').first().trigger('click');
        //     jasmine.Ajax.requests.mostRecent().response({
        //         'status': 404,
        //         'content/type': 'text/html',
        //         'responseText': 'will not use this'
        //     });
        //     expect(failSpy).toHaveBeenCalledWith(jasmine.any(Object), jasmine.any(Object), content);
        // });
        it("should allow a user to specify jQuery selectors to exclude from ajax linking", function() {
            var spy = spyOn($, 'isUrlExternal');
            $frag = $('#wrapper');
            // mock the $.browser.webkit
            $.browser = {
                webkit: 1
            };
            // #wrapper contains an anchor with the .no-ajax class
            $frag.ajaxify({
                excludeSelector: '.no-ajax'
            }).find('a.no-ajax').first().trigger('click');
            expect(spy).not.toHaveBeenCalled();
        });
        it("should allow plugin users to set an ajaxAnchorClicked callback function", function() {
            spyOn($, 'isUrlExternal').and.returnValue(false);
            var ajaxAnchorClickedSpy = jasmine.createSpy('ajaxAnchorClicked');
            $frag = $("#wrapper");
            $frag.ajaxify({
                loadToSelector: 'p.item',
                ajaxAnchorClicked: ajaxAnchorClickedSpy
            }).find('a[href="/some/url"]').first().trigger('click');
            expect(ajaxAnchorClickedSpy).toHaveBeenCalled();
        });
        it("should allow plugin users to set an externalAnchorClicked callback function", function() {
            spyOn($, 'isUrlExternal').and.returnValue(true);
            var externalAnchorClickedSpy = jasmine.createSpy('externalAnchorClicked');
            $frag = $("#wrapper");
            $frag.ajaxify({
                loadToSelector: 'p.item',
                externalAnchorClicked: externalAnchorClickedSpy
            }).find('a[href="http://google.com"]').trigger('click');
            expect(externalAnchorClickedSpy).toHaveBeenCalled();
        });
        it("should allow plugin users to set a url exclude pattern using the exclude option", function() {
            spyOn($, 'isUrlExternal').and.returnValue(false);
            var beforeLoadSpy = jasmine.createSpy('beforeLoad');
            $frag = affix('#menu a.a[href="#some-section"]+a.b[href="/some/page"]');
            $frag.ajaxify({
                beforeLoad: beforeLoadSpy,
                excludeSelector: 'a[href^="#"]'
            }).find('a.a').click();
            expect(beforeLoadSpy).not.toHaveBeenCalled();
            $frag.find('a.b').click();
            expect(beforeLoadSpy).toHaveBeenCalled();
        });
        it("should allow users to set an excludedClicked callback function", function() {
            spyOn($, 'isUrlExternal').and.returnValue(false);
            var excludedClickedSpy = jasmine.createSpy('excludedClicked');
            $frag = affix('#menu a.a[href="#some-section"]+a.b[href="/some/page"]');
            $frag.ajaxify({
                excludedClicked: excludedClickedSpy,
                excludeSelector: 'a.a'
            }).find('a.a').click();
            expect(excludedClickedSpy).toHaveBeenCalled();
        });
        it("should allow users to set a noDeepLinkSelector", function() {
            // spyOn($, 'isUrlExternal').and.returnValue(false);
            var spy = spyOn(window.history, 'pushState');
            var excludedClickedSpy = jasmine.createSpy('excludedClicked');
            $frag = affix('#menu a.no-deep-link[href="#some-section"]+a.deep-link[href="/some/page"]');
            $frag.ajaxify({
                noDeepLinkSelector: 'a.no-deep-link'
            }).find('a.deep-link').click();
            expect(spy).toHaveBeenCalled();
            spy.calls.reset();
            $frag.find('a.no-deep-link').click();
            expect(spy).not.toHaveBeenCalled();
        });
    });
    describe("Events", function() {
        it("should raise an externalAnchorClicked event on the ajaxified when clicked an external link", function() {
            var $frag = affix('#menu a.external[href="http://google.com"]');
            var eventSpy = spyOnEvent('#menu', 'externalAnchorClicked');
            $frag.ajaxify().find('a.external').click();
            expect(eventSpy).toHaveBeenTriggered();
        });
        it("should raise an excludedClicked event on the ajaxified when clicked an excluded link", function() {
            var $frag = affix('#menu a.excluded[href="/some/path"]');
            var eventSpy = spyOnEvent('#menu', 'excludedClicked');
            $frag.ajaxify({
                excludeSelector: '.excluded'
            }).find('a.excluded').click();
            expect(eventSpy).toHaveBeenTriggered();
        });
        it("should raise a afterFail event on the ajaxified when failing to load contenfailing to load content", function() {
            var $frag = affix('#menu a.link[href="/some/path"]');
            var eventSpy = spyOnEvent('#menu', 'afterFail');
            $frag.ajaxify().find('a.link').click();
            jasmine.Ajax.requests.mostRecent().response({
                'status': 404,
                'content/type': 'text/html',
                'responseText': 'will not use this'
            });
            expect(eventSpy).toHaveBeenTriggered();
        });
        it("should raise a afterLoad event on the ajaxified when loading succeeded", function() {
            var $frag = affix('#menu a.link[href="/some/path"]');
            var eventSpy = spyOnEvent('#menu', 'afterLoad');
            $frag.ajaxify().find('a.link').click();
            jasmine.Ajax.requests.mostRecent().response({
                'status': 200,
                'content/type': 'text/html',
                'responseText': 'will not use this'
            });
            expect(eventSpy).toHaveBeenTriggered();
        });
        it("should raise a beforeLoad event on the ajaxified before attempting to load new content", function() {
            var $frag = affix('#menu a.link[href="/some/path"]');
            var eventSpy = spyOnEvent('#menu', 'beforeLoad');
            $frag.ajaxify().find('a.link').click();
            expect(eventSpy).toHaveBeenTriggered();
        });
        it("should raise an ajaxAnchorClicked event on the ajaxified when an internal and not excluded link is clicked", function() {
            var $frag = affix('#menu a.link[href="/some/path"]');
            var eventSpy = spyOnEvent('#menu', 'ajaxAnchorClicked');
            $frag.ajaxify().find('a.link').click();
            expect(eventSpy).toHaveBeenTriggered();
        });
        it("should raise events by default", function() {
            expect($.fn.ajaxify.options.raiseEvents).toBe(true);
        });
        it("should allow a plugin user to disable event raising", function() {
            var $frag = affix('#menu a.link[href="/some/path"]');
            var eventSpy = spyOnEvent('#menu', 'ajaxAnchorClicked');
            $frag.ajaxify({
                raiseEvents: false
            }).find('a.link').click();
            expect(eventSpy).not.toHaveBeenTriggered();
        });
        it("should partialize full internal urls that would fail the urlInternal check", function() {
            var $frag = affix('#element a.homeLink[href="http://testing.dev"]+a.pageLink[href="http://testing.dev/page"]+a.pageLink2[href="http://testing.dev/page/"]+a.external[href="http://google.com"]+a.partial[href="/some-page"]');
            $frag.ajaxify({
                base: 'http://testing.dev'
            });
            expect($frag.find('a.homeLink').attr('href')).toBe('/');
            expect($frag.find('a.pageLink').attr('href')).toBe('/page');
            expect($frag.find('a.pageLink').attr('href')).toBe('/page');
            expect($frag.find('a.external').attr('href')).toBe('http://google.com');
            expect($frag.find('a.partial').attr('href')).toBe('/some-page');
        });
    });
});