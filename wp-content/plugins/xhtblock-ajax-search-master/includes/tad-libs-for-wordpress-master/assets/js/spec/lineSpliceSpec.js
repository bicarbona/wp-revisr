describe("Line splice string functions", function() {

    var input = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        shortInput = 'Some short sentence.';

    describe("lineSpliceByWords", function() {

        it("should be defined on the String prototype", function() {
            expect(String.prototype.lineSpliceByWords).toBeDefined();
        });

        describe("Workings", function() {

            it("should split a string inserting a span each 8 words by default", function() {
                expect(input.lineSpliceByWords()).toBe('<span class="line">Lorem ipsum dolor sit amet, consectetur adipisicing elit,</span><span class="line">sed do eiusmod tempor incididunt ut labore et</span><span class="line">dolore magna aliqua.</span>');
            });

            it("should accept a different number of words per line", function() {
                expect(input.lineSpliceByWords(4)).toBe('<span class="line">Lorem ipsum dolor sit</span><span class="line">amet, consectetur adipisicing elit,</span><span class="line">sed do eiusmod tempor</span><span class="line">incididunt ut labore et</span><span class="line">dolore magna aliqua.</span>');
            });

            it("should allow to specify a different tag", function() {
                expect(input.lineSpliceByWords(4, 'div')).toBe('<div class="line">Lorem ipsum dolor sit</div><div class="line">amet, consectetur adipisicing elit,</div><div class="line">sed do eiusmod tempor</div><div class="line">incididunt ut labore et</div><div class="line">dolore magna aliqua.</div>');
            });

            it("should allow to specify a different class", function() {
                expect(input.lineSpliceByWords(4, 'div', 'cool-line')).toBe('<div class="cool-line">Lorem ipsum dolor sit</div><div class="cool-line">amet, consectetur adipisicing elit,</div><div class="cool-line">sed do eiusmod tempor</div><div class="cool-line">incididunt ut labore et</div><div class="cool-line">dolore magna aliqua.</div>');
            });

            it("should just append the initial and final span to shorter strings", function() {
               expect(shortInput.lineSpliceByWords()).toBe('<span class="line">Some short sentence.</span>');
            });

            it("should allow the specification of a maxiumum length for the returned string", function() {
               expect(input.lineSpliceByWords(4, 'div', 'cool-line', 80)).toBe('<div class="cool-line">Lorem ipsum dolor sit</div><div class="cool-line">amet, consectetur adipisicing elit,</div><div class="cool-line">sed do eiusmod tempor&hellip;</div>');
            });

            it("should allow the specification of the ellipsis char", function() {
               expect(input.lineSpliceByWords(4, 'div', 'cool-line', 80, '[read-more]')).toBe('<div class="cool-line">Lorem ipsum dolor sit</div><div class="cool-line">amet, consectetur adipisicing elit,</div><div class="cool-line">sed do eiusmod tempor[read-more]</div>');
            });

            it("should not append ellipsis to short sentences", function() {
               expect(shortInput.lineSpliceByWords(4, 'div', 'some', 80, '[read-more]')).toBe('<div class="some">Some short sentence.</div>');
            });
        });
    });

    describe("lineSpliceByChars", function() {

        it("should be defined on the String prototype", function() {
            expect(String.prototype.lineSpliceByChars).toBeDefined();
        });
        describe("Workings", function() {

            it("should split a string at most 80 chars by default", function() {
                expect(input.lineSpliceByChars()).toBe('<span class="line">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span><span class="line">incididunt ut labore et dolore magna aliqua.</span>');
            });

            it("should allow for a different number of chars per line", function() {
                expect(input.lineSpliceByChars(50)).toBe('<span class="line">Lorem ipsum dolor sit amet, consectetur</span><span class="line">adipisicing elit, sed do eiusmod tempor incididunt</span><span class="line">ut labore et dolore magna aliqua.</span>')
            });

            it("should allow to specify a different tag", function() {
                expect(input.lineSpliceByChars(50, 'div')).toBe('<div class="line">Lorem ipsum dolor sit amet, consectetur</div><div class="line">adipisicing elit, sed do eiusmod tempor incididunt</div><div class="line">ut labore et dolore magna aliqua.</div>');
            });

            it("should allow to specify a different class", function() {
                expect(input.lineSpliceByChars(50, 'div', 'some')).toBe('<div class="some">Lorem ipsum dolor sit amet, consectetur</div><div class="some">adipisicing elit, sed do eiusmod tempor incididunt</div><div class="some">ut labore et dolore magna aliqua.</div>');
            });

            it("should just append the initial and final span to shorter strings", function() {
               expect(shortInput.lineSpliceByChars()).toBe('<span class="line">Some short sentence.</span>');
            });

            it("should allow the specification of a maxiumum length for the returned string", function() {
                expect(input.lineSpliceByChars(50, 'div', 'some', 80)).toBe('<div class="some">Lorem ipsum dolor sit amet, consectetur</div><div class="some">adipisicing elit, sed do eiusmod tempor&hellip;</div>');
            });

            it("should allow the specification of the ellipsis char", function() {
               expect(input.lineSpliceByChars(50, 'div', 'some', 80, '[read-more]')).toBe('<div class="some">Lorem ipsum dolor sit amet, consectetur</div><div class="some">adipisicing elit, sed do eiusmod tempor[read-more]</div>');
            });

            it("should not append ellipsis to short sentences", function() {
               expect(shortInput.lineSpliceByChars(50, 'div', 'some', 80, '[read-more]')).toBe('<div class="some">Some short sentence.</div>');
            });
        });
    });
});
