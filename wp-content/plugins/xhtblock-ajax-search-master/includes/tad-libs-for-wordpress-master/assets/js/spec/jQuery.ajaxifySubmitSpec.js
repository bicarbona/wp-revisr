describe("jQuery.ajaxifySubmit plugin specs", function() {

    var wrapper, form;

    beforeEach(function() {
        // mock all ajax requests
        jasmine.Ajax.install();
        // create the markup form
        wrapper = affix('#wrapper');
        main = wrapper.affix('#main').html('some lorem');
        form = wrapper.affix('form#searchform');
        form.affix('input.query[type="text"][value="some value"]');
        form.affix('input.submit[type="submit"]');
    });

    afterEach(function() {
        jasmine.Ajax.uninstall();
    });

    describe("Basic plugin setup", function() {

        it("should be available on the jQuery object", function() {
            expect($.fn.ajaxifySubmit).toBeDefined();
        });

        it("should be chainable", function() {
            var $frag = affix('#sandbox');
            expect($frag.ajaxifySubmit()).toBe($frag);
        });

        it("should return a collection if a collection was passed in", function() {
            var $frag = affix('#sandbox');
            $frag.affix('.item');
            $frag.affix('.item');
            $frag.affix('.item');
            expect($frag.find('form').ajaxifySubmit().length).toBe($frag.find('form').length);
        });

        it("offers a defaults object on the ajaxifySubmit namespace", function() {
            expect($.fn.ajaxifySubmit.options).toBeDefined();
        });

    });

    describe("Plugin functions", function() {
        it("should clear the field on focus on default", function() {
            expect(form.ajaxifySubmit().find('input[type="text"]').focus().val()).toBe('');
        });
    });
    it("should restore the placeholder on blur by default", function() {
        expect(form.ajaxifySubmit().find('input[type="text"]').focus().val()).toBe('');
        expect(form.find('input[type="text"]').blur().val()).toBe('some value');
    });
    it("should live search with 3 or more chars by default", function() {
        var beforeLoadSpy = jasmine.createSpy('beforeLoad');
        form.ajaxifySubmit({
            beforeLoad: beforeLoadSpy,
            liveSearchTimeout: 0
        }).find('input[type="text"]').val('a').keyup();
        expect(beforeLoadSpy).not.toHaveBeenCalled();
        form.find('input[type="text"]').val('ab').keyup();
        expect(beforeLoadSpy).not.toHaveBeenCalled();
        form.find('input[type="text"]').val('abc').keyup();
        expect(beforeLoadSpy).toHaveBeenCalled();
        // test the query
        expect(jasmine.Ajax.requests.mostRecent().url).toBe('/?s=abc');
    });

    describe("Plugin default options", function() {
        it("should get query from input[type=text] by default", function() {
            expect($.fn.ajaxifySubmit.options.getQueryFromSelector).toBe('input[type=text]');
        });
        it("should separate query words with plus sign by default", function() {
            expect($.fn.ajaxifySubmit.options.querySeparator).toBe('+');
        });
        it("should use /?s= as query pathname by default", function() {
            expect($.fn.ajaxifySubmit.options.queryPathname).toBe('/?s=');
        });
        it("should filter new content using the #main-content selector by default", function() {
            expect($.fn.ajaxifySubmit.options.loadFromSelector).toBe('#main-content');
        });
        it("should load new content into the #main by default", function() {
            expect($.fn.ajaxifySubmit.options.loadToSelector).toBe('#main');
        });
        it("should not exclude any selector by default", function() {
            expect($.fn.ajaxifySubmit.options.excludeSelector).toBe('');
        });
        it("should live search by default", function() {
            expect($.fn.ajaxifySubmit.options.liveSearch).toBe(true);
        });
        it("should live search after 3 or more chars", function() {
            expect($.fn.ajaxifySubmit.options.liveSearchChars).toBe(3);
        });
    });
    describe("Plugin options", function() {
        it("should allow plugin users to set a submitClicked callback function", function() {
            submitClickedSpy = jasmine.createSpy('submitClicked');
            form.ajaxifySubmit({
                submitClicked: submitClickedSpy
            }).submit();
            expect(submitClickedSpy).toHaveBeenCalled();
        });
        it("should allow plugin users to set a beforeLoad callback function", function() {
            beforeLoadSpy = jasmine.createSpy('beforeLoad');
            form.ajaxifySubmit({
                beforeLoad: beforeLoadSpy
            }).submit();
            expect(beforeLoadSpy).toHaveBeenCalled();
        });
        it("should allow plugin users to set the query pathname", function() {
            form.ajaxifySubmit({
                queryPathname: '/search/'
            }).submit();
            // test the query
            expect(jasmine.Ajax.requests.mostRecent().url).toBe('/search/some+value');
        });
        it("should allow plugin users to set the input to set the selector for the input to get the query from", function() {
            // trigger the method that will make the ajax request
            form.ajaxifySubmit({
                getQueryFromSelector: '.query'
            }).submit();
            // test the query
            expect(jasmine.Ajax.requests.mostRecent().url).toBe('/?s=some+value');
        });
        it("should allow plugin users to set the input to set the query separator string", function() {
            // trigger the method that will make the ajax request
            form.ajaxifySubmit({
                querySeparator: '-'
            }).submit();
            // test the query
            expect(jasmine.Ajax.requests.mostRecent().url).toBe('/?s=some-value');
        });
        it("should allow plugin users to set the input to set the selector to filter the response", function() {
            var response = '<div class="wrapper"><div class="more-wrapper"><div id="content1">lorem ipsum</div><div id="theContent">dolor sit</div></div></div>';
            var afterLoadSpy = jasmine.createSpy('afterLoadSpy');
            // trigger the method that will make the ajax request
            form.ajaxifySubmit({
                loadFromSelector: '#theContent',
                afterLoad: afterLoadSpy
            }).submit();
            // test the query
            expect(jasmine.Ajax.requests.mostRecent().url).toBe('/?s=some+value');
            jasmine.Ajax.requests.mostRecent().response({
                "status": 200,
                'responseText': response
            });
            expect(afterLoadSpy).toHaveBeenCalledWith(jasmine.any(Object), jasmine.any(Object), '<div id="theContent">dolor sit</div>');
        });
        it("should allow plugin users to set the afterFail callback function", function() {
            var afterFailSpy = jasmine.createSpy('afterFailSpy');
            // trigger the method that will make the ajax request
            form.ajaxifySubmit({
                afterFail: afterFailSpy
            }).submit();
            // test the query
            expect(jasmine.Ajax.requests.mostRecent().url).toBe('/?s=some+value');
            jasmine.Ajax.requests.mostRecent().response({
                "status": 404
            });
            expect(afterFailSpy).toHaveBeenCalledWith(jasmine.any(Object), jasmine.any(Object), 'some lorem');
        });
        it("should allow plugin users to set an onFocus callback function", function() {
            var onFocusSpy = jasmine.createSpy('onFocus');
            // trigger the method that will make the ajax request
            form.ajaxifySubmit({
                onFocus: onFocusSpy
            }).find('input[type="text"]').focus();
            expect(onFocusSpy).toHaveBeenCalled();
        });
        it("should allow plugin users to set an onBlur callback function", function() {
            var onBlurSpy = jasmine.createSpy('onBlur');
            // trigger the method that will make the ajax request
            form.ajaxifySubmit({
                onBlur: onBlurSpy
            }).find('input[type="text"]').blur();
            expect(onBlurSpy).toHaveBeenCalled();
        });
        it("should allow plugin users to decide if blurring will restore placeholder", function() {
            // trigger the method that will make the ajax request
            var input = form.find('input[type="text"]');
            form.ajaxifySubmit({
                restorePlaceholderOnBlur: false
            });
            input.val('not some value here').blur();
            expect(input.val()).toBe('not some value here');
        });
        it("should allow plugin users to decide if focus will clear", function() {
            // trigger the method that will make the ajax request
            var input = form.find('input[type="text"]');
            form.ajaxifySubmit({
                clearOnFocus: false
            });
            input.focus();
            expect(input.val()).toBe('some value');
        });
        it("should allow plugin users to disable live search", function() {
            var beforeLoadSpy = jasmine.createSpy('beforeLoad');
            form.ajaxifySubmit({
                beforeLoad: beforeLoadSpy,
                liveSearch: false
            }).find('input[type="text"]').val('abcde').keyup();
            expect(beforeLoadSpy).not.toHaveBeenCalled();
        });
        it("should allow plugin users to set the number of chars that will trigger the live search to less than 3", function() {
            var beforeLoadSpy = jasmine.createSpy('beforeLoad');
            form.ajaxifySubmit({
                beforeLoad: beforeLoadSpy,
                liveSearchChars: 2,
                liveSearchTimeout: 0
            }).find('input[type="text"]').val('a').keyup();
            expect(beforeLoadSpy).not.toHaveBeenCalled();
            form.find('input[type="text"]').val('ab').keyup();
            expect(beforeLoadSpy).toHaveBeenCalled();
            // test the query
            expect(jasmine.Ajax.requests.mostRecent().url).toBe('/?s=ab');
        });
        it("should allow plugin users to set the number of chars that will trigger the live search to more than 3", function() {
            var beforeLoadSpy = jasmine.createSpy('beforeLoad');
            form.ajaxifySubmit({
                beforeLoad: beforeLoadSpy,
                liveSearchChars: 5,
                liveSearchTimeout: 0
            }).find('input[type="text"]').val('abc').keyup();
            expect(beforeLoadSpy).not.toHaveBeenCalled();
            form.find('input[type="text"]').val('abcd').keyup();
            expect(beforeLoadSpy).not.toHaveBeenCalled();
            form.find('input[type="text"]').val('abcde').keyup();
            expect(beforeLoadSpy).toHaveBeenCalled();
            // test the query
            expect(jasmine.Ajax.requests.mostRecent().url).toBe('/?s=abcde');
        });
    });
});