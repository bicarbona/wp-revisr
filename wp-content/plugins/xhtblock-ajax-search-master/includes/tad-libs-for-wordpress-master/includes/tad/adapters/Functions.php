<?php
namespace tad\adapters;

class Functions implements \tad\interfaces\FunctionsAdapter
{
    public function __call($function, $arguments)
    {
        if (!is_string($function) or !function_exists($function)) {
            return;
        }
        return call_user_func_array($function, $arguments);
    }
}
