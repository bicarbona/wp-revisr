<?php
namespace tad\wrappers\headway;

use tad\utils\Arr;
use tad\adapters\Functions;
use tad\interfaces\FunctionsAdapter;

class GlobalSettings extends \tad\wrappers\SerializedOption
{
    const OPTION_NAME = 'headway_option_group_general';

    protected $prefix;

    public function __construct($prefix = '', $isSiteOption = false, \tad\interfaces\FunctionsAdapter $functions = null)
    {
        if (!is_string($prefix)) {
            throw new \BadMethodCallException("Prefix must be a string", 1);
        }
        $this->prefix = $prefix;
        parent::__construct(self::OPTION_NAME, false, $functions);
    }
    public static function on($optionName, $isSiteOption = false, \tad\interfaces\FunctionsAdapter $functions = null)
    {
        return new self($prefix, $isSiteOption, $functions);
    }
    protected function loadPropertiesFrom($arr)
    {
        if ($this->prefix == '') {
            parent::loadPropertiesFrom($arr);
            return;
        }
        // no option in db
        if ($arr == array()) {
            $this->val = null;
            // no point in going on
            return;
        }
        // if the value is not an associative array return
        if (!Arr::isAssoc($arr)) {
            return;
        }
        $this->val = null;
        $buffer = array();
        foreach ($arr as $key => $value) {
            // key should begin with the prefix
            if (0 === strpos($key, $this->prefix)) {
                $deprefixedKey = str_replace($this->prefix, '', $key);
                $buffer[$deprefixedKey] = $value;
            }
        }
        if (count($buffer) > 0) {
            $this->val = $buffer;
            $this->_data = Arr::camelBackKeys($buffer);
        }
    }
}
