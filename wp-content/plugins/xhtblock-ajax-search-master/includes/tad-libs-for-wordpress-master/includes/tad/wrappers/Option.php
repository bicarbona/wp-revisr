<?php
namespace tad\wrappers;

use tad\utils\Arr;
use tad\utils\Str;
use tad\interfaces\FunctionsAdapter;
use tad\adapters\Functions;

class Option
{
    protected $optionId;
    protected $isSiteOption;
    protected $functions;
    protected $loadedValue;
    protected $originalKeys;
    protected $val;
    protected $suffix = '';
    protected $underscoreNames;

    public function __construct($optionId, $isSiteOption = false, $underscoreNames = false, \tad\interfaces\FunctionsAdapter $functions = null)
    {
        if (!is_string($optionId)) {
            throw new \BadMethodCallException("Option Id must be a string", 1);
        }
        if (!is_bool($isSiteOption)) {
            throw new \BadMethodCallException("Site option flag must be a boolean value", 2);
        }
        $this->underscoreNames = $underscoreNames;
        if (is_null($functions)) {
            $functions = new \tad\adapters\Functions();
        }
        $this->functions = $functions;
        // check to see if WordPress stores such an option
        // and eventually retrieve it
        $this->optionId = $optionId;
        $this->isSiteOption = $isSiteOption;
        if ($isSiteOption) {
            $this->suffix = '_site';
        }
        $this->val = $this->loadedValue = $this->loadOption($optionId, $isSiteOption);
        $this->loadPropertiesFrom($this->loadedValue);
    }
    public static function on ($optionId, \tad\interfaces\FunctionsAdapter $functions = null)
    {
        return new self($optionId, false, false, $functions);
    }
    public function __destruct()
    {
        $functionName = sprintf('update%s_option', $this->suffix);
        // for array options restore the original keys
        if (Arr::isAssoc($this->val)) {
            $buffer = array();
            foreach ($this->val as $key => $value) {
                // default to camelBack key
                $originalKey = $key;
                // get the original key if any
                if (isset($this->originalKeys[$key])) {
                    $originalKey = $this->originalKeys[$key];
                }
                // store the value with the original key
                $buffer[$originalKey] = $value;
            }
            $this->val = $buffer;
        }
        $this->functions->$functionName($this->optionId, $this->val);
    }
    protected function loadOption($optionId)
    {
        $functionName = sprintf('get%s_option', $this->suffix);
        return $this->functions->$functionName($optionId, array());
    }
    protected function loadPropertiesFrom($value)
    {
        // if it's not an associative array then return
        if (!Arr::isAssoc($value)) {
            return;
        }
        // store the original keys and save their
        // camelBack counterpart
        $this->originalKeys = array();
        // reset the val array
        $this->val = array();
        $originalKeys = array_keys($value);
        foreach ($originalKeys as $key) {
            // which function to use for key naming?
            $funcName = 'camelBack';
            if ($this->underscoreNames) {
                $funcName = 'underscore';
            }
            // original ['some key' => 'value']
            $newKey = Str::$funcName($key);
            // ['someKey' => 'some key']
            $this->originalKeys[$newKey] = $key;
            // ['someKey' => 'value']
            $this->val[$newKey] = $value[$key];
        }
    }
    public function __get($key)
    {
        if ($key == 'val') {
            return $this->val;
        }
        // if it's an explicit property then return it
        if (isset($this->$key)) {
            return $this->$key;
        }
        if (!isset($this->val[$key])) {
            return null;
        }
        return $this->val[$key];
    }
    public function __set($key, $value)
    {
        if ($key == 'val') {
            $this->val = $value;
            return;
        }
        // avoid trying to set a key in a non array option
        if (!is_array($this->val) and !Arr::isAssoc($this->val)) {
            return;
        }
        $this->val[$key] = $value;
    }
    public function reset()
    {
        $this->val = $this->loadedValue;
    }
}
