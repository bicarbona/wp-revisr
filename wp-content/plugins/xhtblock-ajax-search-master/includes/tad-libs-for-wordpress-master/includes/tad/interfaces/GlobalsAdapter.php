<?php
namespace tad\interfaces;

interface GlobalsAdapter
{
    public function __call($name, $args);
}
