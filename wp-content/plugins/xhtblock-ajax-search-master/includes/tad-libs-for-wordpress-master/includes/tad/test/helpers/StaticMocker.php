<?php
namespace tad\test\helpers;

class StaticMocker
{
    protected static $methods;
    protected static $listener;

    public static function __callStatic($func, $args)
    {
        self::_maybeCallListener($func, $args);
        return self::$methods[$func];
    }
    public static function _addMethod($func, $returnValue = null)
    {
        self::$methods[$func] = $returnValue;
    }
    public static function _reset()
    {
        self::$methods = array();
        self::_setListener();
    }
    public static function _setListener($listener = null)
    {
        self::$listener = $listener;
    }
    public static function _getListener()
    {
        return self::$listener;
    }
    protected static function _maybeCallListener($func, $args)
    {
        if (!is_null(self::$listener) and method_exists(self::$listener, $func)) {
            call_user_method_array($func, self::$listener, $args);
        }
    }
}
