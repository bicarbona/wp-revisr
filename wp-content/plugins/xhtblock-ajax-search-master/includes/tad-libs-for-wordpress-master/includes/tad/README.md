# theAverageDev libraries for WordPress

A set of classes meant to make test-driven and object-oriented WordPress plugin and themes development easier.  
The classes are aimed at developers wishing to test their code and stick to some standards and best practices.

## Adapters

### Functions adapter
The libraries contains an adapter class meant to act as middle man between a plugin or theme and the globally defined functions and objects; WordPress core and many plugins make heavy use of globally defined functions and that would force someone wishing to use one of those functions to run a real WordPress local server to do some testing. The adapter will abstract that dependency allowing classes to be tested in isolation.  
Given the below class

    <?php

    class SomeClass
    {
        protected $functions;

        public function __construct(\tad\interfaces\FunctionsAdapter $functions = null)
        {
            if(is_null($functions)) {
                $functions = new \tad\adapters\Functions.php
            }
            $this->functions = $functions;
        }

        public function someMethod()
        {
            // some logic

            $this->functions->add_action('init', array($this, 'anotherMethod'));

            // some more logic
        }

        public function anotherMethod()
        {
            // do something here
        }
    }

The PHPUnit test case might be

    <?php

    use SomeClass;

    class SomeClassTest extends \PHPUnit_Framework_TestCase
    {
        protected $sut;

        public function setUp()
        {
            $mockFunctions = $this->getMock('\tad\interfaces\FunctionsAdapter');
            $this->sut = new SomeClass($this->mockFunctions);
        }

        testSomeMethodWillAddActionAtInit()
        {
            $this->mockFunctions->expects($this->once())
                                ->method('add_action')
                                ->with('init', array($sut, 'anotherMethod'));
            $this->sut->someMethod;
        }
    }

### Superglobals adapter
While mocking global variables is not difficult I appreciate the possibility to check and test the access to those variables via mocks and expectations and that's not possible if the superglobals are accessedd directly.  
    
    <?php

    class SomeClass
    {
        // 'sg' stands for 'superglobals'
        protected $sg;

        public function __construct(\tad\interfaces\GlobalsAdapter $sg = null)
        {
            if(is_null($sg)) {
                $sg = new tad\adapters\Globals.php
            }
            $this->sg = $sg;
        }

        public function testableMethod()
        {
            // get the 'foo' variable in the $GLOBALS array
            $foo = $this->sg->globals('foo');
            // get the $_SERVER array
            $server = $this->sg->server();
        }

        public function notThatTestableMethod()
        {
            $foo = $GLOBALS['foo'];
            $server = $SERVER;
        }
    }

Superglobals can be accessed using a function named like their lowercase name, hence <code>$GLOBALS</code> with <code>Globals::globals()</code>, <code>$_SERVER</code> with <code>Globals::server()</code> and so on.

## Specialized wrappers
While the <code>Functions</code> and the <code>Globals</code> adadpter will allow access to all what's global in a WordPress session I'm implementing, as I see fit and feel the need, more classes with less god-like attitude and single responsibilities.

### Theme supports
This class will take care of adding and removin theme support for theme features; the usage is quite simple

    $themeSupport = new \tad\wrappers\ThemeSupport();
    $themeSupport->add('some-feature');
    $themeSupport->remove('some-feature');

The class instance will take care to hook into the <code>after_setup_theme</code> action hook and handling the support options.

## Utilities

## Str (the string manipulator)
The <code>\tad\utils\Str</code> class exposed static methods to manipulate anc convert strings in theis hypen or dash separated counterparts as well as camel case and camel back formats.