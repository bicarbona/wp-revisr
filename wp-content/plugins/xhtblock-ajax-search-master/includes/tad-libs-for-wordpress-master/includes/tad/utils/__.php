<?php
namespace tad\utils;

class __ extends \brianhaveri\__{
    public static function __callStatic($name, $arguments)
    {
        self::maybeAddMethods();
        return parent::__callStatic($name, $arguments);
    }
    public function __call($name, $arguments)
    {
        self::maybeAddMethods();
        return $this->__call($name, $arguments);
    }

    private static $addedMethods = false;
    private static function maybeAddMethods()
    {
        if (self::$addedMethods) {
            return;
        }
        foreach (glob(dirname(__FILE__) . '/*.php') as $file) {
            if ($file == __FILE__) {
                continue;
            }
            $className = '\tad\utils\\' . basename($file, '.php');
            if (method_exists($className, 'addToUnderscore')) {
                $className::addToUnderscore();
            }
        }
        self::$addedMethods = true;
    }
}