=== theAverageDeve libraries ===
Contributors:      theAverageDev (Luca Tumedei)
Donate link:       https://github.com/lucatume/tad-libs-for-wordpress
Tags:              library
Requires at least: 3.5.1
Tested up to:      3.5.1
Stable tag:        0.1.0
License:           GPLv2 or later
License URI:       http://www.gnu.org/licenses/gpl-2.0.html

A set of libraries to make WordPress theme and plugin development easier.

== Description ==
The libraries are born out of my need to rely on utilitity functions and classes, mainly classes, to be able to develop themes and plugins in a consistently and reliable object-oriented manner.

= Manual Installation =

1. Upload the entire `/theaveragedeve-libraries` directory to the `/mu-plugins/` directory in your WordPress content folder.
