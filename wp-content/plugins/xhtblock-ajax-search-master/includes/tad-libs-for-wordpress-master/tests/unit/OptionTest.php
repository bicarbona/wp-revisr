<?php

use \tad\wrappers\Option as Option;

class OptionTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructWillThrowForNonStringOptionId()
    {
        $this->setExpectedException('BadMethodCallException', 1);
        new Option(23);
    }
    public function testConstructWillThrowForNonBooleanSiteOptionValue()
    {
        $this->setExpectedException('BadMethodCallException', 2);
        new Option('someOption', 23);
    }
    public function testConstructWillCallGetOptionForNonSiteOptions()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'get_option'));
        $mockf->expects($this->once())->method('get_option')->with('someOption');
        new Option('someOption', false, false, $mockf);
    }
    public function testConstructWillCallGetSiteOptionForSiteOptions()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'get_site_option'));
        $mockf->expects($this->once())->method('get_site_option')->with('someOption');
        new Option('someOption', true, false, $mockf);
    }
    public function testSingleValueOptionsWillBeAccessibleViaTheValueProperty()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'get_option'));
        $mockf->expects($this->once())->method('get_option')->with('someOption')->will($this->returnValue(24));
        $sut = new Option('someOption', false, false, $mockf);
        $this->assertEquals(24, $sut->val);
    }
    public function testArrayValueOptionsWillExposeArrayAs_ValueProperty()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'get_option'));
        $r = array('foo', 'baz', 'bar');
        $mockf->expects($this->once())->method('get_option')->with('someOption')->will($this->returnValue($r));
        $sut = new Option('someOption', false, false, $mockf);
        $this->assertEquals($r, $sut->val);
    }
    public function testAssociativeArrayValuePropertiesWillPopulatePropertiesWithKeys()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'get_option'));
        $r = array('some' => 'value', 'more' => 'anotherValue', 'bar' => 23);
        $mockf->expects($this->once())->method('get_option')->with('someOption')->will($this->returnValue($r));
        $sut = new Option('someOption', false, false, $mockf);
        $this->assertObjectHasAttribute('val', $sut);
        $this->assertEquals($r, $sut->val);
        $this->assertEquals('value', $sut->some);
        $this->assertEquals('anotherValue', $sut->more);
        $this->assertEquals(23, $sut->bar);
    }
    public function testTryingToGetAMissingPropertyWillReturnNull()
    {
        $sut = new Option('someOption');
        $this->assertNull($sut->some);
    }
    public function testConstructingOverAMissingOptionWillCreateAnEmptyArrayOption()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'get_option'));
        $mockf->expects($this->once())->method('get_option')->with('someOption', array())->will($this->returnValue(array()));
        $sut = new Option('someOption', false, false, $mockf);
        $this->assertObjectHasAttribute('val',$sut);
        $this->assertNotNull($sut->val);
        $this->assertEquals(array(), $sut->val);
    }
    public function testConstructingOverAMissingSiteOptionWillCreateAnEmptyArrayOption()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'get_site_option'));
        $mockf->expects($this->once())->method('get_site_option')->with('someOption', array())->will($this->returnValue(array()));
        $sut = new Option('someOption', true, false, $mockf);
        $this->assertObjectHasAttribute('val',$sut);
        $this->assertNotNull($sut->val);
        $this->assertEquals(array(), $sut->val);
    }
    public function testSettingTheValueWillSetTheValue()
    {
        $sut = new Option('someOption');
        $sut->val = 'newValue';
        $this->assertEquals('newValue', $sut->val);
    }
    public function testSettingAPropertyWillSetThePropertyValue()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'get_option'));
        $mockf->expects($this->once())->method('get_option')->with('someOption', array())->will($this->returnValue(array()));
        $sut = new Option('someOption', false, false, $mockf);
        $sut->someValue = 'newValue';
        $this->assertEquals('newValue', $sut->someValue);
    }

    public function testSettingAPropertyWillSetTheValueForTheKeyInValueProperty()
    {
        $sut = new Option('someOption');
        $sut->val = array('some' => 'value');
        $sut->some = 'newValue';
        $this->assertEquals('newValue', $sut->val['some']);
    }
    public function testSettingTheValueWillUpdateTheProperties()
    {
        $sut = new Option('someOption');
        $sut->val = array('some' => 'value');
        $this->assertEquals('value', $sut->val['some']);
        $sut->val = array('some' => 'newValue');
        $this->assertEquals('newValue', $sut->val['some']);
    }
    public function testDestructWillCallUpdateOption()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'update_option'));
        $arr = array('foo' => 22, 'baz' => 'bar');
        $mockf->expects($this->once())->method('update_option')->with('someOption', $arr);
        $sut = new Option('someOption', false, false, $mockf);
        $sut->val = $arr;
        // remove all references to the $sut object
        $sut = null;
    }
    public function testDestructWillCallUpdateSiteOption()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'update_site_option'));
        $arr = array('foo' => 22, 'baz' => 'bar');
        $mockf->expects($this->once())->method('update_site_option')->with('someOption', $arr);
        $sut = new Option('someOption', true, false, $mockf);
        $sut->val = $arr;
        // remove all references to the $sut object
        $sut = null;
    }
    public function testResetWillRestoreOptionToLoadedValue()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'get_option'));
        $arr = array('foo' => 22, 'baz' => 'bar');
        $mockf->expects($this->once())->method('get_option')->with('someOption')->will($this->returnValue($arr));
        $sut = new Option('someOption', false, false, $mockf);
        $sut->val = array();
        $sut->foo = 'baz';
        $this->assertEquals('baz', $sut->foo);
        $sut->reset();
        $this->assertEquals($arr, $sut->val);
    }
    public function testSingleValueOptionsWillBeSavedAsSingleValues()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'get_option', 'update_option'));
        $val = 'someValue';
        $mockf->expects($this->once())->method('get_option')->with('someOption')->will($this->returnValue($val));
        $mockf->expects($this->once())->method('update_option')->with('someOption', $val);
        $sut = new Option('someOption', false, false, $mockf);
        $sut = null;
    }
    public function testSettingPropertyOtherThanValueInNonArrayOptionWillNotSetAnything()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'get_option'));
        $val = 'someValue';
        $mockf->expects($this->once())->method('get_option')->with('someOption')->will($this->returnValue($val));
        $sut = new Option('someOption', false, false, $mockf);
        $sut->some = 'newValue';
        $this->assertEquals($val,$sut->val);
    }
    public function testLoadingAnOptionWithNonCamelBackKeysWillCamelBackTheKeys()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'get_option'));
        $val = array('some fancy key' => 'someValue', 'some-key' => 'someOtherValue', 'some_little_key' => 23);
        $mockf->expects($this->once())->method('get_option')->with('someOption')->will($this->returnValue($val));
        $sut = new Option('someOption', false, false, $mockf);
        $value = $sut->val;
        $this->assertEquals(array('someFancyKey', 'someKey', 'someLittleKey'), array_keys($value));
    }
    public function testDestructWillCallUpdateOptionWithOriginalKeys()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call','get_option', 'update_option'));
        $val = array('some fancy key' => 'someValue', 'some-key' => 'someOtherValue', 'some_little_key' => 23);
        $mockf->expects($this->once())->method('get_option')->with('someOption')->will($this->returnValue($val));
        $mockf->expects($this->once())->method('update_option')->with('someOption', $val);
        $sut = new Option('someOption', false, false, $mockf);
        $sut = null;
    }
    public function testLoadingAnOptionWithNonUnderscoreKeysWillUnderscoreTheKeys()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call', 'get_option'));
        $val = array('some fancy key' => 'someValue', 'some-key' => 'someOtherValue', 'some_little_key' => 23);
        $mockf->expects($this->once())->method('get_option')->with('someOption')->will($this->returnValue($val));
        $sut = new Option('someOption', false, true, $mockf);
        $value = $sut->val;
        $this->assertEquals(array('some_fancy_key', 'some_key', 'some_little_key'), array_keys($value));
    }
    public function testDestructWillCallUpdateOptionWithOriginalKeysForUnderscoreNotationToo()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call','get_option', 'update_option'));
        $val = array('some fancy key' => 'someValue', 'some-key' => 'someOtherValue', 'some_little_key' => 23);
        $mockf->expects($this->once())->method('get_option')->with('someOption')->will($this->returnValue($val));
        $mockf->expects($this->once())->method('update_option')->with('someOption', $val);
        $sut = new Option('someOption', false, true, $mockf);
        $sut = null;
    }
    public function testStaticMethodOnWillReturnOptionObject()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call','get_option'));
        $val = array('some fancy key' => 'someValue', 'some-key' => 'someOtherValue', 'some_little_key' => 23);
        $exp = '\tad\wrappers\Option';
        $this->assertInstanceOf($exp, Option::on('someOption'));
    }
    public function testStaticMethodOnWillAllowFluent()
    {
        $mockf = $this->getMock('\tad\interfaces\FunctionsAdapter', array('__call','get_option'));
        $val = array('some fancy key' => 'someValue', 'some-key' => 'someOtherValue', 'some_little_key' => 23);
        $mockf->expects($this->once())->method('get_option')->with('someOption')->will($this->returnValue($val));
        $this->assertEquals(23, Option::on('someOption', $mockf)->someLittleKey);
    }
}
