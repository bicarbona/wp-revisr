<?php

use tad\wrappers\headway\VEPanel as Sut;
// needed to spoof the checks
class Headway
{

}
class VEPanelTest extends \tad\test\cases\TadLibTestCase
{
    protected $className;
    protected $file;

    public function setUp()
    {
        $this->className = 'SomeClass';
    } 
    public function testConstructWillCallAddAnActionToAfterSetupTheme()
    {
        $mockf = $this->getMockFunctions(array('add_action'));
        $sut = $this->getMockBuilder('\tad\wrappers\headway\VEPanel')->disableOriginalConstructor()->getMock();
        $mockf->expects($this->once())->method('add_action')->with('after_setup_theme', array($sut, 'register'));
        $className = 'SomeClass';
        $sut->__construct($this->className, $mockf);
    }
    public function testRegisterWillAddAddAndActionToHeadwayHook()
    {
       $mockf = $this->getMockFunctions(array('add_action'));
        $tag = 'headway_visual_editor_display_init';
       $mockf->expects($this->at(1))->method('add_action')->with($tag, $this->anything(), $this->anything());
       $sut = new Sut($this->className, $mockf);
       $sut->register();
    }
    public function testRegisterWillReturnTrueUponSuccessfulActionHooking()
    {
       $sut = new Sut($this->className);
       $this->assertTrue($sut->register());
    }
    public function testOnStaticMethodWillAllowFluentInterface()
    {
        $mockf = $this->getMockFunctions(array('add_action'));
        $mockf->expects($this->once())->method('add_action');
        $className = 'SomeClass';
        $this->assertInstanceOf('\tad\wrappers\headway\VEPanel', Sut::on($className, $mockf));
    }
  }