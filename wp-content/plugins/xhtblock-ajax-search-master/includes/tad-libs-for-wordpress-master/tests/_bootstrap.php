<?php
$projectRoot = dirname(dirname(__FILE__));
// Add the plugin namespace to the autolaoding classes
spl_autoload_register(function ($class) {
    if (!preg_match("/^tad/", $class)) {
        return;
    }
    // \some\fancy\name
    $class = preg_replace("/^\\\\*\\w*\\\\*/um", "", $class);
    // fancy\name
    $pathElements = explode('\\', $class);
    $path = implode(DIRECTORY_SEPARATOR, $pathElements);
    // fancy/name on *NIX, fancy\name in WIN
    $path = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . $path . '.php';
    $path = preg_replace('/tad-libs-for-wordpress/', 'tad-libs-for-wordpress/includes/tad', $path);
    include $path;
    // root/fancy/name.php
});
spl_autoload_register(function($class){
    $components = explode('\\', $class);
    $path = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $components) . '.php';
    if (file_exists($path)) {
       include_once $path; 
    }
});
