module.exports = function(grunt) {

    // Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                stripBanners: true,
                banner: '/*! <%= pkg.title %> - v<%= pkg.version %>\n' +
                    ' * <%= pkg.homepage %>\n' +
                    ' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
                    ' * Licensed GPLv2+' +
                    ' */\n'
            },
            multi_image: {
                src: [
                    'assets/js/src/multi-image.js'
                ],
                dest: 'assets/js/multi-image.js'
            },
            jquery_ajaxify: {
                src: [
                    'assets/js/src/jQuery-ajaxify.js'
                ],
                dest: 'assets/js/jQuery-ajaxify.js'
            },
            jquery_ajaxifySubmit: {
                src: [
                    'assets/js/src/jQuery-ajaxifySubmit.js'
                ],
                dest: 'assets/js/jQuery-ajaxifySubmit.js'
            },
            breakpoints: {
                src: [
                    'assets/js/src/breakpoints.js'
                ],
                dest: 'assets/js/breakpoints.js'
            },
            helpers: {
                src: [
                    'assets/js/src/lineSplice.js'
                ],
                dest: 'assets/js/tad_helpers.js'
            }
        },
        jshint: {
            all: [
                'Gruntfile.js',
                'assets/js/src/**/*.js',
                'assets/js/test/**/*.js'
            ],
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                boss: true,
                eqnull: true,
                globals: {
                    exports: true,
                    module: false
                }
            }
        },
        uglify: {
            all: {
                files: {
                    'assets/js/multi-image.min.js': ['assets/js/multi-image.js'],
                    'assets/js/jQuery-ajaxify.min.js': ['assets/js/jQuery-ajaxify.js'],
                    'assets/js/jQuery-ajaxifySubmit.min.js': ['assets/js/jQuery-ajaxifySubmit.js'],
                    'assets/js/breakpoints.min.js': ['assets/js/breakpoints.js'],
                    'assets/js/tad_helpers.min.js': ['assets/js/tad_helpers.js']
                },
                options: {
                    banner: '/*! <%= pkg.title %> - v<%= pkg.version %>\n' +
                        ' * <%= pkg.homepage %>\n' +
                        ' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
                        ' * Licensed GPLv2+' +
                        ' */\n',
                    mangle: {
                        except: ['jQuery']
                    },
                    sourceMap: true
                }
            }
        },
        test: {
            files: ['assets/js/test/**/*.js']
        },

        sass: {
            all: {
                options: {
                    sourcemap: true
                },
                files: {
                    'assets/css/multi-image.css': 'assets/css/sass/multi-image.scss'
                }
            }
        },

        autoprefixer: {
            options: {

            },
            multi_image: {
                src: 'assets/css/multi-image.css',
                dest: 'assets/css/multi-image.css'
            }
        },

        cssmin: {
            options: {
                banner: '/*! <%= pkg.title %> - v<%= pkg.version %>\n' +
                    ' * <%= pkg.homepage %>\n' +
                    ' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
                    ' * Licensed GPLv2+' +
                    ' */\n'
            },
            minify: {
                expand: true,

                cwd: 'assets/css/',
                src: ['multi-image.css'],

                dest: 'assets/css/',
                ext: '.min.css'
            }
        },
        watch: {

            sass: {
                files: ['assets/css/sass/*.scss'],
                tasks: ['sass', 'cssmin'],
                options: {
                    debounceDelay: 500
                }
            },

            scripts: {
                files: ['assets/js/src/**/*.js', 'assets/js/vendor/**/*.js', 'assets/js/spec/*.js'],
                tasks: ['jshint', 'jasmine', 'concat', 'uglify'],
                options: {
                    debounceDelay: 500
                }
            }
        },
        clean: {
            main: ['release/<%= pkg.version %>']
        },
        copy: {
            // Copy the plugin to a versioned release directory
            main: {
                src: [
                    '**',
                    '!node_modules/**',
                    '!release/**',
                    '!.git/**',
                    '!.sass-cache/**',
                    '!css/src/**',
                    '!js/src/**',
                    '!img/src/**',
                    '!Gruntfile.js',
                    '!package.json',
                    '!.gitignore',
                    '!.gitmodules'
                ],
                dest: 'release/<%= pkg.version %>/'
            }
        },
        compress: {
            main: {
                options: {
                    mode: 'zip',
                    archive: './release/theaveragedeve_libraries.<%= pkg.version %>.zip'
                },
                expand: true,
                cwd: 'release/<%= pkg.version %>/',
                src: ['**/*'],
                dest: 'theaveragedeve_libraries/'
            }
        },
        jasmine: {
            src: 'assets/js/src/*.js',
            options: {
                vendor: [
                'assets/js/vendor/jQuery/jquery.min.js',
                'assets/js/vendor/jasmine/jasmine-jquery.js',
                'assets/js/vendor/jasmine/jasmine-ajax.js',
                'assets/js/vendor/jasmine/jasmine-fixture.js',
                'assets/js/vendor/jQuery/jquery.urlInternal.min.js',
                'assets/js/vendor/Modernizr/modernizr.min.js'
                ],
                specs: 'assets/js/spec/*.js',
                keepRunner: true,
                '--web-security' : false,
                '--local-to-remote-url-access' : true,
                '--ignore-ssl-errors' : true
                }
        }
    });

    // Load other tasks
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-autoprefixer');

    // Default task.

    grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'sass', 'autoprefixer', 'cssmin']);
    grunt.registerTask('build', ['default', 'clean', 'copy', 'compress']);
    grunt.registerTask('test', ['jshint', 'jasmine']);
    grunt.util.linefeed = '\n';
};