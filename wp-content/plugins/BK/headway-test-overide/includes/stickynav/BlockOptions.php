<?php
namespace stickynav;

    class BlockOptions extends \HeadwayNavigationBlockOptions
{
    // override \HeadwayNavigationBlockOptions::modify_arguments
    public function modify_arguments($args = false)
    {
        // call \HeadwayNavigationBlockOptions::modify_arguments
        parent::modify_arguments($args);

        // add the stickiness option in the orientation tab
        $this->inputs['orientation']['stickiness'] = array(
            'type' => 'checkbox',
            'name' => 'stickyness',
            'label' => 'Sticky',
            'default' => true,
            'tooltip' => 'When scrolled past the navigation block will stick to the top of the page'
            );
                $this->inputs['orientation']['someoption'] = array(
            'type' => 'text',
            'name' => 'someoption',
            'label' => 'Sticky',
            'default' => true,
            'tooltip' => 'When scrolled past the navigation block will stick to the top of the page'
            );


    }
}