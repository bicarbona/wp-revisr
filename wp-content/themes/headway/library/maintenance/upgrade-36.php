<?php
/**
 * Pre-3.6
 *
 * - Merge all design settings into the one option
 **/
add_action('headway_do_upgrade_36', 'headway_do_upgrade_36');
function headway_do_upgrade_36() {

	$combined_design_settings = array();

	//Fetch all options in wp_options and get old Headway design options
	foreach ( wp_load_alloptions() as $option => $option_value ) {

		if ( strpos($option, 'headway_option_group_design-editor-group') !== 0 )
			continue;

		$combined_design_settings = array_merge($combined_design_settings, get_option($option));

	}

	HeadwaySkinOption::set('properties', $combined_design_settings, 'design');

	HeadwayElementsData::merge_core_default_design_data();

	HeadwayMaintenance::output_status('Successfully Upgraded Design Editor Data');

	return true;

}