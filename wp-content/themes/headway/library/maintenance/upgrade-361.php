<?php
/**
 * 3.6.1
 *
 * Do 3.6 design conversion if the Headway 3.6 design options is the same as the default.  This is to fix the bad 3.6 upgrade bug
 **/
add_action('headway_do_upgrade_361', 'headway_do_upgrade_361');
function headway_do_upgrade_361() {

	if ( HeadwayOption::$current_skin == HEADWAY_DEFAULT_SKIN && HeadwaySkinOption::get('properties', 'design', array()) == HeadwayElementsData::get_default_data() ) {

		$combined_design_settings = array();

		//Fetch all options in wp_options and get old Headway design options
		foreach ( wp_load_alloptions() as $option => $option_value ) {

			if ( strpos($option, 'headway_option_group_design-editor-group') !== 0 )
				continue;

			$combined_design_settings = array_merge($combined_design_settings, get_option($option));

		}

		if ( !empty($combined_design_settings) ) {

			HeadwaySkinOption::set('properties', $combined_design_settings, 'design');

			HeadwayElementsData::merge_default_design_data(HeadwayElementsData::get_default_data(), 'core-361-upgrade-fix');

		}

	}

	HeadwayMaintenance::output_status('Successfully Upgraded Design Editor Data');

}