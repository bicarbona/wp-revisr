<?php
class HeadwayMaintenance {

	public static $available_upgrades = array(
		'3.6',
		'3.6.1',
		'3.7',
		'3.7.1'
	);

	/**
	 * Over time, there may be issues to be corrected between updates or naming conventions to be changed between updates.
	 * All of that will be processed here.
	 **/
	static function do_upgrades() {

		$headway_settings = get_option('headway', array('version' => 0));
		$db_version = $headway_settings['version'];

		if ( $db_version == HEADWAY_VERSION ) {

			return self::output_status('<h3>Your Headway installation is already up to date.  Enjoy your week!</h3>');

		}

		/* Add current version to upgrades if it's not there so the basic upgrade routine is still ran */
		if ( !in_array(HEADWAY_VERSION, self::$available_upgrades) ) {
			self::$available_upgrades[] = HEADWAY_VERSION;
		}

		if ( !headway_get('do-upgrade') ) {

			foreach ( self::$available_upgrades as $possible_upgrade ) {

				if ( version_compare( $db_version, $possible_upgrade, '<' ) ) {

					$_GET['do-upgrade'] = $possible_upgrade;
					break;

				}

			}

		}

		/* Do specified upgrade routine */
		if ( $upgrade_in_progress = headway_get('do-upgrade') ) {

			$version_filename = str_replace( '.', '', $upgrade_in_progress );

			if ( version_compare( $db_version, $upgrade_in_progress, '<' ) ) {

				self::start_upgrade($upgrade_in_progress);

				if ( file_exists(HEADWAY_LIBRARY_DIR . '/maintenance/upgrade-' . $version_filename . '.php') ) {
					require_once HEADWAY_LIBRARY_DIR . '/maintenance/upgrade-' . $version_filename . '.php';
				}

				do_action('headway_do_upgrade_' . $version_filename);

				self::after_upgrade($upgrade_in_progress);

			}

		}

		return true;

	}


	public static function setup_upgrade_environment() {

		/* Remove all admin notices */
		remove_all_actions('admin_notices');

		@ignore_user_abort( true );
		@set_time_limit( 0 );

		/* Attempt to raise memory limit to max */
		@ini_set( 'memory_limit', apply_filters( 'headway_memory_limit', WP_MAX_MEMORY_LIMIT ) );

		/* Enable output flushing */
//		@header( 'X-Accel-Buffering: no' );
//
//		@apache_setenv( 'no-gzip', 1 );
//		@ini_set( 'zlib.output_compression', 0 );
//		@ini_set( 'implicit_flush', 1 );
//
//		@ob_implicit_flush( 1 );


	}


	public static function output_status( $text ) {

		echo '<p>' . $text . '</p>';

//		@ob_flush();
//		@flush();

		return true;

	}


	public static function start_upgrade($version) {

		update_option( 'headway_upgrading', 'upgrading' );

		self::output_status('<h3>Currently Upgrading to ' . $version . '</h3>');

	}


	public static function after_upgrade($version) {

		/* Flush caches */
		do_action( 'headway_db_upgrade' );

		/* Update the version here. */
		$headway_settings            = get_option( 'headway', array( 'version' => 0 ) );
		$headway_settings['version'] = $version;

		update_option( 'headway', $headway_settings );
		delete_option( 'headway_upgrading' );

		self::output_status('Finished upgrade to ' . $version);

		/* Redirect to next upgrade if available */
		$index_of_current_version = array_search($version, self::$available_upgrades);

		if ( isset(self::$available_upgrades[$index_of_current_version + 1]) ) {

			$next_upgrade = self::$available_upgrades[$index_of_current_version + 1];

			self::output_status('<h4>Loading Next Upgrade: ' . $next_upgrade . '</h4>');
			self::output_status('<meta http-equiv="refresh" content="2;URL=' . admin_url('admin.php?page=headway-upgrade&do-upgrade=' . $next_upgrade) . '" />');

			die();

		} else {

			self::output_status('<h3>Upgrade successful!  Have a great day!</h3>');
			self::output_status('<meta http-equiv="refresh" content="2;URL=' . admin_url('admin.php?page=headway-whats-new') . '" />');

			die();

		}

	}

}