<?php
class HeadwayTemplates {


	public static function get($id) {

		$all_skins = self::get_all(true);

		return headway_get($id, $all_skins);

	}


	public static function get_all($associative = false) {

		$skins = array(
			array(
				'id' => 'base',
				'name' => 'Base',
				'author' => 'Headway Themes',
				'version' => HEADWAY_VERSION,
				'image-url' => headway_url() . '/screenshot.png',
				'description' => null
			)
		);

		$installed_skins = HeadwayOption::get_group('skins');

		if ( !$installed_skins )
			HeadwayOption::set_group('skins', array());
				
		if ( !empty($installed_skins) && is_array($installed_skins) ) {

			foreach ( $installed_skins as $installed_skin_id => $installed_skin ) {

				$skins[] = array_merge(array(
					'id' => $installed_skin_id, 
					'description' => null,
					'version' => null,
					'author' => null
				), $installed_skin);

			}

		}

		/* Resize all images */
		foreach ( $skins as $skin_index => $skin ) {

			$skins[$skin_index]['image-url'] = headway_resize_image(headway_get('image-url', $skin), 220, 190);

			if ( !$skins[$skin_index]['image-url'] || is_wp_error($skins[$skin_index]['image-url']) )
				$skins[$skin_index]['image-url'] = headway_get('image-url', $skin);

		}
		
		if ( $associative ) {

			$associative_skins = array();

			foreach ( $skins as $skin )
				$associative_skins[$skin['id']] = $skin;

			return $associative_skins;

		}

		return $skins;

	}


	public static function get_active() {

		$current_skin_id = HeadwayOption::get('current-skin', 'general', HEADWAY_DEFAULT_SKIN);
		$all_skins = self::get_all(true);

		return headway_get($current_skin_id, $all_skins, headway_get(HEADWAY_DEFAULT_SKIN, $all_skins));

	}


	public static function get_active_id() {

		$active_skin = self::get_active();

		return $active_skin['id'];

	}


	public static function add() {



	}


	public static function delete() {



	}


}