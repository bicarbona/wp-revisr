<?php
class HeadwayDataPortability {


	public static function export_skin(array $info) {

		global $wpdb;

		do_action('headway_before_export_skin');

		$wp_options_prefix = 'headway_|template=' . HeadwayOption::$current_skin . '|_';

		$skin = array(
			'hw-version' => HEADWAY_VERSION,
			'name' => $info['name'],
			'author' => $info['author'],
			'image-url' => $info['image-url'],
			'version' => $info['version'],
			'data_wp_options' => $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->options WHERE option_name LIKE '%s'", $wp_options_prefix . '%')),
			'data_wp_postmeta' => $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->postmeta WHERE meta_key LIKE '%s'", '_hw_|template=' . HeadwayOption::$current_skin . '|_%')),
			'data_hw_layout_meta' => $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->hw_layout_meta WHERE template = '%s'", HeadwayOption::$current_skin)),
			'data_hw_wrappers' => $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->hw_wrappers WHERE template = '%s'", HeadwayOption::$current_skin)),
			'data_hw_blocks' => $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->hw_blocks WHERE template = '%s'", HeadwayOption::$current_skin))
		);

		/* Spit the file out */
		return self::to_json('Headway Template - ' . $info['name'] . ' ' . $info['version'], 'skin', $skin);

	}


	public static function install_skin(array $skin) {

		$skins = HeadwayOption::get_group('skins');

		/* Remove image definitions */
			if ( isset($skin['image-definitions']) )
				unset($skin['image-definitions']);

		/* Skin ID */
			$original_skin_id = strtolower(str_replace(' ', '-', $skin['name']));

			$skin_id = $original_skin_id;
			$skin_name = $skin['name'];

			$skin_unique_id_counter = 0;

		/* Check if skin already exists.  If it does, change ID and skin name */
			while ( HeadwayOption::get($skin_id, 'skins') || get_option('headway_|template=' . $skin_id . '|_option_group_general') ) {

				$skin_unique_id_counter++;
				$skin_id = $original_skin_id . '-' . $skin_unique_id_counter;

				$skin_name = $skin['name'] . ' ' . $skin_unique_id_counter;

			}

			$skin['id'] = $skin_id;
			$skin['name'] = $skin_name;

		/* Send skin to DB */
			HeadwayOption::set($skin['id'], $skin, 'skins');

		/* Change current skin ID to the newly added skin so we can populate data */
			HeadwayOption::$current_skin = $skin['id'];
			HeadwayLayoutOption::$current_skin = $skin['id'];

		/* Process the install */
			if ( !headway_get('hw-version', $skin) || version_compare(headway_get('hw-version', $skin), '3.7', '<') ) {
				$skin = self::process_install_skin_pre37($skin);
			} else {
				$skin = self::process_install_skin($skin);
			}

		/* Change $current_skin back just to be safe */
			HeadwayOption::$current_skin = HeadwayTemplates::get_active_id();
			HeadwayLayoutOption::$current_skin = HeadwayTemplates::get_active_id();

		return $skin;

	}


		public static function process_install_skin_pre37(array $skin) {

			/* Set up skin options that way when it's activated it looks right */
				/* Install templates */
				if ( $skin_templates = headway_get('templates', $skin) )
					HeadwaySkinOption::set_group('templates', $skin_templates);

					/* Assign templates */
						if ( !empty($skin['templates']['assignments']) ) {

							foreach ( $skin['templates']['assignments'] as $layout_id => $template_id ) {

								/* Change layout ID separators */
								if ( strpos($layout_id, 'template-') !== 0 )
									$layout_id = str_replace('-', HeadwayLayout::$sep, $layout_id);

								HeadwayLayoutOption::set($layout_id, 'template', $template_id);

							}

						}

				/* Install layouts (blocks, wrappers, and flags */
					$wrapper_id_mapping = array();
					$block_id_mapping = array();

					foreach ( $skin['layouts'] as $layout_id => $layout_data ) {

						/* Change layout ID separators */
							if ( strpos($layout_id, 'template-') !== 0 )
								$layout_id = str_replace('-', HeadwayLayout::$sep, $layout_id);

						/* Install Wrappers */
							foreach ( $layout_data['wrappers'] as $wrapper_id => $wrapper_data ) {

								$wrapper_data['position'] = array_search($wrapper_id, array_keys($layout_data['wrappers']));

								$wrapper_data['settings'] = array(
									'fluid' => headway_get('fluid', $wrapper_data),
									'fluid-grid' => headway_get('fluid-grid', $wrapper_data),
									'columns' => headway_get('columns', $wrapper_data),
									'column-width' => headway_get('column-width', $wrapper_data),
									'gutter-width' => headway_get('gutter-width', $wrapper_data),
									'use-independent-grid' => headway_get('use-independent-grid', $wrapper_data)
								);

								$new_wrapper = HeadwayWrappersData::add_wrapper($layout_id, $wrapper_data);

								if ( $new_wrapper && !is_wp_error($new_wrapper)  ) {
									$wrapper_id_mapping[HeadwayWrappers::format_wrapper_id($wrapper_id)] = $new_wrapper;
								}

							}

						/* Install Blocks */
							foreach ( $layout_data['blocks'] as $block_id => $block_data ) {

								$block_data['wrapper'] = headway_get(HeadwayWrappers::format_wrapper_id($block_data['wrapper']), $wrapper_id_mapping);

								$new_block = HeadwayBlocksData::add_block($layout_id, $block_data);

								if ( $new_block && !is_wp_error($new_block) ) {
									$block_id_mapping[$block_id] = $new_block;
								}

							}

					}

				/* Setup mirroring */
					foreach ( $skin['layouts'] as $layout_id => $layout_data ) {

						/* Change layout ID separators */
						if (strpos($layout_id, 'template-') !== 0)
							$layout_id = str_replace('-', HeadwayLayout::$sep, $layout_id);

						foreach ($layout_data['wrappers'] as $wrapper_id => $wrapper_data) {

							$wrapper_to_update = $wrapper_id_mapping[HeadwayWrappers::format_wrapper_id($wrapper_id)];

							if (!$mirror_id = headway_get('mirror-wrapper', $wrapper_data))
								continue;

							$mirror_id = headway_get(HeadwayWrappers::format_wrapper_id($mirror_id), $wrapper_id_mapping);

							HeadwayWrappersData::update_wrapper($wrapper_to_update, array(
								'mirror_id' => $mirror_id
							));

						}

						foreach ($layout_data['blocks'] as $block_id => $block_data) {

							$block_to_update = $block_id_mapping[$block_id];

							if (!$mirror_id = headway_get('mirror-block', headway_get('settings', $block_data, array())))
								continue;

							$mirror_id = headway_get($mirror_id, $block_id_mapping);

							HeadwayBlocksData::update_block($block_to_update, array(
								'mirror_id' => $mirror_id
							));

						}

					}

			/* Install design data */
				/* Sort the block and wrapper mappings by descending number that way when we do a simple recursive find and replace the small block IDs won't mess up the larger block IDs.
				   Example: Replacing block-1 before block-11 is replaced would be bad news */
				krsort($block_id_mapping);
				krsort($wrapper_id_mapping);

				foreach ( $block_id_mapping as $old_block_id => $new_block_id ) {
					$skin['element-data'] = headway_str_replace_json('block-' . $old_block_id, 'block-' . $new_block_id, $skin['element-data']);
				}

				foreach ( $wrapper_id_mapping as $old_wrapper_id => $new_wrapper_id ) {
					$skin['element-data'] = headway_str_replace_json('wrapper-' . $old_wrapper_id, 'wrapper-' . $new_wrapper_id, $skin['element-data']);
				}

				HeadwaySkinOption::set('properties', $skin['element-data'], 'design');
				HeadwaySkinOption::set('live-css', stripslashes($skin['live-css']));

			/* Set merge flag that way the next time they save it won't screw up the styling */
				HeadwaySkinOption::set('merged-default-design-data-core', true, 'general');

			/* Set wrapper defaults */
				if ( !empty($skin['wrapper-defaults']) && is_array($skin['wrapper-defaults']) ) {

					HeadwaySkinOption::set('columns', headway_get('columns', $skin['wrapper-defaults'], HeadwayWrappers::$default_columns));
					HeadwaySkinOption::set('columns-width', headway_get('columns', $skin['wrapper-defaults'], HeadwayWrappers::$default_columns));
					HeadwaySkinOption::set('gutter-width', headway_get('columns', $skin['wrapper-defaults'], HeadwayWrappers::$default_columns));

				}

			return $skin;

		}



		public static function process_install_skin(array $skin) {

			return HeadwayDataSnapshots::process_rollback($skin, $skin['id']);

		}


	public static function export_block_settings($block_id) {

		/* Set up variables */
			$block = HeadwayBlocksData::get_block($block_id);

		/* Check if block exists */
			if ( !$block )
				die('Error: Could not export block settings.');

		/* Spit the file out */
			return self::to_json('Block Settings - ' . HeadwayBlocksData::get_block_name($block), 'block-settings', array(
				'id' => $block_id,
				'type' => $block['type'],
				'settings' => $block['settings'],
				'styling' => HeadwayBlocksData::get_block_styling($block)
			));

	}


	public static function export_layout($layout_id) {

		/* Set up variables */
			if ( !$layout_name = HeadwayLayout::get_name($layout_id) )
				die('Error: Invalid layout.');

			$layout = array(
				'name' => $layout_name,
				'blocks' => HeadwayBlocksData::get_blocks_by_layout($layout_id)
			);

		/* Convert all mirrored blocks into original blocks by pulling their mirror target's settings */
			/* Loop through each block in the template and check if it's mirrored.  If it is, replace it with the block that it's mirroring */
			foreach ( $layout['blocks'] as $layout_block_index => $layout_block ) {

				if ( !$mirrored_block = HeadwayBlocksData::get_block_mirror($layout_block) )
					continue;

				$layout['blocks'][$layout_block_index] = $mirrored_block;

			}

		/* Spit the file out */
		return self::to_json('Headway Layout - ' . $layout_name, 'layout', $layout);

	}


	/**
	 * Convert array to JSON file and force download.
	 *
	 * Images will be converted to base64 via HeadwayDataPortability::encode_images()
	 **/
	public static function to_json($filename, $data_type = null, $array) {

		if ( !$array['data-type'] = $data_type )
			die('Missing data type for HeadwayDataPortability::to_json()');

		$array['image-definitions'] = self::encode_images($array);

		header('Content-Disposition: attachment; filename="' . $filename . '.json"');
		header('Content-Type: application/json');
		header('Pragma: no-cache');

		echo json_encode($array);

	}


		/**
		 * Convert all images to base64.
		 *
		 * This method is recursive.
		 **/
		public static function encode_images(&$array, $images = null) {

			if ( !$images )
				$images = array();

			foreach ( $array as $key => $value ) {

				if ( is_array($value) ) {

					$images = array_merge($images, self::encode_images($array[$key], $images));
					continue;

				} else if ( !is_serialized($value) && is_string($value) ) {

					$image_matches = array();

					/* PREG_SET_ORDER makes the $image_matches array make more sense */
					preg_match_all('/([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif))/i', $value, $image_matches, PREG_SET_ORDER);

					/* Go through each image in the string and download it then base64 encode it and replace the URL with variable */
					foreach ( $image_matches as $image_match ) {

						if ( !count($image_match) )
							continue;

						$image_request = wp_remote_get($image_match[0]);

						if ( $image_request && $image_contents = wp_remote_retrieve_body($image_request) ) {

							$image = array(
								'base64_contents' => base64_encode($image_contents),
								'mime_type' => $image_request['headers']['content-type']
							);

							/* Add base64 encoded image to image definitions. */
								/* Make sure that the image isn't already in the definitions.  If it is, $possible_duplicate will be the key/ID to the image */
								if ( !$possible_duplicate = array_search($image, $images) )
									$images['%%IMAGE_REPLACEMENT_' . (count($images) + 1) . '%%'] = $image;

							/* Replace the URL with variable that way it can be replaced with uploaded image on import.  If $possible_duplicate isn't null/false, then use it! */
								$variable = $possible_duplicate ? $possible_duplicate : '%%IMAGE_REPLACEMENT_' . (count($images)) . '%%';
								$array[$key] = str_replace($image_match[0], $variable, $array[$key]);

						}

					}

				}

			}

			return $images;

		}


	/**
	 * Convert base64 encoded image into a file and move it to proper WP uploads directory.
	 **/
	public static function decode_image_to_uploads($base64_string) {

		/* Make sure user has permissions to edit in the Visual Editor */
			if ( !HeadwayCapabilities::can_user_visually_edit() )
				return;

		/* Create a temporary file and decode the base64 encoded image into it */
			$temporary_file = wp_tempnam();
			file_put_contents($temporary_file, base64_decode($base64_string));

		/* Use wp_check_filetype_and_ext() to figure out the real mimetype of the image.  Provide a bogus extension and then we'll use the 'proper_filename' later. */
			$filename = 'headway-imported-image.jpg';
			$file_information = wp_check_filetype_and_ext($temporary_file, $filename);

		/* Construct $file array which is similar to a PHP $_FILES array.  This array must be a variable since wp_handle_sideload() requires a variable reference argument. */
			if ( headway_get('proper_filename', $file_information) )
				$filename = $file_information['proper_filename'];

			$file = array(
				'name' => $filename,
				'tmp_name' => $temporary_file
			);

		/* Let WordPress move the image and spit out the file path, URL, etc.  Set test_form to false that way it doesn't verify $_POST['action'] */
			$upload = wp_handle_sideload($file, array('test_form' => false));

			/* If there's an error, be sure to unlink/delete the temporary file in case wp_handle_sideload() doesn't. */
			if ( isset($upload['error']) )
				@unlink($temporary_file);

			return $upload;

	}


}