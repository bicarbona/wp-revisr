<?php
if ( !class_exists('HeadwayPinBoardCoreBlock') ) {

	headway_register_block('HeadwayPinBoardCoreBlock', headway_url() . '/library/blocks/pin-board');
	class HeadwayPinBoardCoreBlock extends HeadwayBlockAPI {


		public $id = 'pin-board';

		public $name = 'Pin Board';

		public $options_class = 'HeadwayPinBoardCoreBlockOptions';


		public static function init() {

			add_action('wp_ajax_headway_pin_board_infinite_scroll', array(__CLASS__, 'infinite_scroll_content'));
			add_action('wp_ajax_nopriv_headway_pin_board_infinite_scroll', array(__CLASS__, 'infinite_scroll_content'));

		}


		public static function enqueue_action($block_id) {

			/* CSS */
			wp_enqueue_style('headway-pin-board', headway_url() . '/library/blocks/pin-board/css/pin-board.css');

			/* JS */
			wp_enqueue_script('headway-pin-board', headway_url() . '/library/blocks/pin-board/js/pin-board.js', array('jquery'));

			/* Variables */
			wp_localize_script('headway-pin-board', 'HeadwayPinBoard', array(
				'ajaxURL' => admin_url('admin-ajax.php'),
				'isArchive' => is_archive(),
				'isSearch' => is_search()
			));

		}


		public static function dynamic_js($block_id, $block = false) {

			if ( !$block )
				$block = HeadwayBlocksData::get_block($block_id);

			$infinite_scroll = intval(parent::get_setting($block, 'infinite-scroll', true));

			$js = "
			jQuery(document).ready(function() {
				setupPinBoardBlock({
					blockID: '" . $block_id . "',
					effects: {
						hoverFocus: " . (parent::get_setting($block, 'hover-focus', false) ? 'true' : 'false') . ",
						infiniteScroll: ( !!HeadwayPinBoard.isArchive || !!HeadwayPinBoard.isSearch ) ? 0 : " . $infinite_scroll . "
					},
					columns: " . parent::get_setting($block, 'columns', 3) . ",
					columnsSmartphone: " . parent::get_setting($block, 'columns-smartphone', 2) . ",
					gutterWidth: " . parent::get_setting($block, 'gutter-width', 15) . "
				});
			});
			";

			return $js;

		}


		public static function dynamic_css($block_id) {

			return '#block-' . $block_id . ' .pin-board-pin { margin-bottom: ' . parent::get_setting($block_id, 'pin-bottom-margin', 15) . 'px; }';

		}


		/**
		 * Anything in here will be displayed when the block is being displayed.
		 **/
		public function content($block) {

			global $wp_query;

			$columns = parent::get_setting($block, 'columns', 3);
			$approx_pin_width = (HeadwayBlocksData::get_block_width($block) / $columns);

			/* Element Visibility */
				$show_images = parent::get_setting($block, 'show-images', true);
				$show_titles = parent::get_setting($block, 'show-titles', true);

				/* Meta */
					$show_author = parent::get_setting($block, 'show-author', false);
					$show_datetime = parent::get_setting($block, 'show-datetime', false);
					$datetime_verb = parent::get_setting($block, 'datetime-verb', 'Posted');
					$relative_times = parent::get_setting($block, 'relative-times', true);

				/* Content */
					$show_continue = parent::get_setting($block, 'show-continue', false);
					$content_to_show = parent::get_setting($block, 'content-to-show', 'excerpt');
					$show_text_when_no_image = parent::get_setting($block, 'show-text-if-no-image', false);
					$titles_position = parent::get_setting($block, 'titles-position', 'below');
					$titles_link_to_post = parent::get_setting($block, 'titles-link-to-post', true);

			/* Images */
					$crop_images_vertically = parent::get_setting($block, 'crop-vertically', false);
					$image_click_action = parent::get_setting($block, 'image-click-action', 'post');

					if ( $image_click_action == 'popup' ) {
						add_thickbox();
					}

			/* Social Stuff */
				/* Pinterest */
					$show_pinterest_button = parent::get_setting($block, 'show-pinterest-button', false);

				/* Twitter */
					$show_twitter_button = parent::get_setting($block, 'show-twitter-button', false);

					$twitter_username = parent::get_setting($block, 'twitter-username', null);
					$twitter_hashtag = parent::get_setting($block, 'twitter-hashtag', null);

				/* Facebook */
					$show_facebook_button = parent::get_setting($block, 'show-facebook-button', false);
					$facebook_button_verb = parent::get_setting($block, 'facebook-button-verb', 'like');
			/* End Social Stuff */

			$infinite_scroll = (is_archive() || is_search()) ? 0 : intval(parent::get_setting($block, 'infinite-scroll', true));

			/* Setup Query */
				// If it's an archive or search page, we need to use its query as the base and extend where possible
				if (is_archive() || is_search()) {
					$query_args = $wp_query->query_vars;
				} else {
					$query_args = array();
				}

				/* Pagination */
					$paged_var = get_query_var('paged') ? get_query_var('paged') : get_query_var('page');

					if ( (parent::get_setting($block, 'paginate', true) || $infinite_scroll) && (headway_get('pin-board-page') || $paged_var) )
						$query_args['paged'] = headway_get('pin-board-page') ? headway_get('pin-board-page') : $paged_var;

				/* Post type */
					if ( !headway_get('post_type', $query_args) )
						$query_args['post_type'] = parent::get_setting($block, 'post-type', 'post');

				/* Pin limit */
					$query_args['posts_per_page'] = parent::get_setting($block, 'pins-per-page', 10);

				/* Order */
				//	$query_args['orderby'] = parent::get_setting($block, 'order-by', 'date');
				//	$query_args['order'] = parent::get_setting($block, 'order', 'DESC');

			/* Custom query overrides */
				if ( parent::get_setting($block, 'mode', 'default') == 'custom' ) {

					/* Categories */
					/* Now taxonomies */
						if ( !is_archive() && !is_search() ) {

							$terms_list = parent::get_setting($block, 'categories', false);

							if ( !$terms_list ) {
								$tax_slug   = HeadwayBlockAPI::get_setting($block, 'taxonomies', 'category');
								$terms_list = HeadwayPinBoardBlockOptions::get_tax_terms($tax_slug, true);
							}

							$query_args['tax_query'] = array(
								array(
									'taxonomy' => parent::get_setting($block, 'taxonomies', 'category'),
									'field'    => 'slug',
									'terms'    => $terms_list,
									'operator' => parent::get_setting($block, 'categories-mode', 'include') == 'exclude' ? 'NOT IN' : 'IN'
								),
							);

						}

					/* Author Filter */
						if ( is_array(parent::get_setting($block, 'author')) )
							$query_args['author'] = trim(implode(',', parent::get_setting($block, 'author')), ', ');

				}

				/* Query! */
				$original_wp_query = $wp_query;
				$wp_query = new WP_Query($query_args);

				global $paged; /* Set paged to the proper number because WordPress pagination SUCKS!  ANGER! */
				$paged = $paged_var;
				/* End Query Setup */

				echo '<div class="pin-board" data-pin-board-page="' . headway_get('paged', $query_args, 1) . '">';

				while ( $wp_query->have_posts() ) {

					$wp_query->the_post();

					/* If only images are being shown and there's no thumbnail, then don't show the pin. */
					if ( !($show_images && has_post_thumbnail()) && !$content_to_show && !$show_titles && !$show_text_when_no_image )
						continue;

					echo '<div class="pin-board-pin ' . implode(' ', get_post_class()) . '">';

						/* Titles above */
							if ( $show_titles && $titles_position == 'above') {

								echo '<h3 class="entry-title">';
								if ($titles_link_to_post) {
									echo '<a href="' . get_permalink() . '">' . get_the_title() . '</a>';
								} else {
									echo get_the_title();
								}
								echo '</h3>';

							}
						/* End Titles below */

						/* Thumbnail */
							if ( has_post_thumbnail() && $show_images ) {

								$thumbnail_id = get_post_thumbnail_id();

								$thumbnail_width = $approx_pin_width + 30; /* Add a 30px buffer to insure that image will be large enough */

								//$crop_vertically
								if ( $crop_images_vertically ) {

									$thumbnail_height = round($approx_pin_width * 0.75);

									$thumbnail_object = wp_get_attachment_image_src($thumbnail_id, 'full');
									$thumbnail_url = headway_resize_image($thumbnail_object[0], $thumbnail_width, $thumbnail_height);
									$full_image_url = $thumbnail_object[0];

								} else {

									$thumbnail_object = wp_get_attachment_image_src($thumbnail_id, 'full');
									$thumbnail_url = headway_resize_image($thumbnail_object[0], $thumbnail_width);
									$full_image_url = $thumbnail_object[0];

								}

								do_action('headway_before_pin_thumbnail');

								echo '<div class="pin-board-pin-thumbnail">';

									if ( $image_click_action == 'post' ) {

										echo '<a href="' . get_permalink() . '" class="post-thumbnail" title="' . get_the_title() . '">';
											echo '<img src="' . esc_url($thumbnail_url) . '" alt="' . get_the_title() . '" />';
										echo '</a><!-- .post-thumbnail -->';

									} elseif ($image_click_action == 'popup') {

										echo '<a href="' . esc_url($full_image_url) . '" class="thickbox post-thumbnail" rel="pinboard-'.$block['id'].'" title="' . get_the_title() . '">';
											echo '<img src="' . esc_url($thumbnail_url) . '" alt="' . get_the_title() . '" />';
										echo '</a><!-- .post-thumbnail -->';

									} else {

										echo '<a class="post-thumbnail"><img src="' . esc_url($thumbnail_url) . '" alt="' . get_the_title() . '" /></a>';

									}
									if ( $show_pinterest_button || $show_twitter_button || $show_facebook_button ) {

										echo '<div class="pin-board-pin-thumbnail-social">';

											if ( $show_facebook_button )
												self::facebook_button(get_permalink(), $facebook_button_verb);

											if ( $show_twitter_button )
												self::twitter_button(get_permalink(), get_the_title(), $twitter_username, $twitter_hashtag);

											if ( $show_pinterest_button ) {

												$full_size_image = wp_get_attachment_image_src($thumbnail_id, 'full');
												$full_size_image_url = $full_size_image[0];

												self::pinterest_button(get_permalink(), $full_size_image_url);

											}

										echo '</div><!-- .pin-board-pin-thumbnail-social -->';

									}

								echo '</div><!-- .pin-board-pin-thumbnail -->';

								do_action('headway_after_pin_thumbnail');

							}
						/* End Thumbnail */

						/* Titles below */
							if ( $show_titles && $titles_position == 'below') {

								echo '<h3 class="entry-title">';
								if ($titles_link_to_post) {
									echo '<a href="' . get_permalink() . '">' . get_the_title() . '</a>';
								} else {
									echo get_the_title();
								}
								echo '</h3>';

							}
						/* End Titles below */

						/* Meta */
							if ( $show_author || $show_datetime ) {

								global $authordata;

								do_action('headway_before_pin_meta');

								echo '<div class="entry-meta">';

									if ( $show_datetime ) {
										echo '<span class="entry-date published" title="' . get_the_time('c') . '">' . ($datetime_verb ? $datetime_verb . ' ' : '') . self::relative_time($relative_times) . '</span> ';
									}

									if ( $show_author ) {
										echo '<em class="author-by">by</em> <a class="author-link fn nickname url" href="' . get_author_posts_url($authordata->ID) . '" title="View all entries by ' . $authordata->display_name . '">' . $authordata->display_name . '</a>';
									}

								echo '</div><!-- .entry-meta -->';

								do_action('headway_after_pin_meta');

							}
						/* End Meta */

						/* Excerpts/Content */
								do_action('headway_before_pin_content');
								echo '<div class="pin-board-pin-text entry-content">';
								if ( ($show_text_when_no_image && !has_post_thumbnail()) || ($content_to_show && !$show_text_when_no_image)) {
									if ($content_to_show == 'excerpt') {
											add_filter('excerpt_more', array(__CLASS__, 'excerpt_more'));
											the_excerpt();
											remove_filter('excerpt_more', array(__CLASS__, 'excerpt_more'));
									} elseif ( $content_to_show == 'content') {
										the_content();
									}
								}
								echo '</div><!-- .pin-board-pin-text -->';
								do_action('headway_after_pin_content');

						/* End Excerpts */


						/* Backup social buttons if no image is present */
							if ( (!has_post_thumbnail() || !$show_images) && ($show_twitter_button || $show_facebook_button) ) {

								echo '<div class="pin-board-pin-social">';

									if ( $show_twitter_button )
										self::twitter_button(get_permalink(), get_the_title(), $twitter_username, $twitter_hashtag);

									if ( $show_facebook_button )
										self::facebook_button(get_permalink(), $facebook_button_verb);

								echo '</div><!-- .pin-board-pin-social -->';

							}
						/* End backup social buttons */

					echo '</div><!-- .pin-board-pin -->';
				} // End while loop

			echo '</div><!-- .pin-board -->';

			if ( parent::get_setting($block, 'paginate', true) || $infinite_scroll ) {

				do_action('headway_before_pin_board_pagination');
				self::pagination($wp_query, $infinite_scroll,parent::get_setting($block, 'enumerate', false));
				do_action('headway_after_pin_board_pagination');
			}
			$wp_query = $original_wp_query;

		}


			public static function infinite_scroll_content() {

				$block = HeadwayBlocksData::get_block(headway_get('block'));

				if ( is_array($block) && $block['type'] == 'pin-board' )
					self::content($block);

				die();

			}


		/**
		 * Register elements to be edited by the Headway Design Editor
		 **/

		public function setup_elements() {

			$this->register_block_element(array(
				'id' => 'pin',
				'name' => 'Pin',
				'selector' => '.pin-board-pin',
				'properties' => array('background', 'borders', 'padding', 'rounded-corners', 'box-shadow')
			));

			$this->register_block_element(array(
				'id' => 'pin-title',
				'name' => 'Pin Title',
				'selector' => '.pin-board-pin .entry-title, .pin-board-pin .entry-title a',
				'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
				'states' => array(
					'Hover' => '.pin-board-pin .entry-title a:hover',
				)
			));

			$this->register_block_element(array(
				'id' => 'pin-meta',
				'name' => 'Pin Meta',
				'selector' => '.pin-board-pin .entry-meta',
				'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow')
			));

			$this->register_block_element(array(
				'id' => 'pin-text',
				'name' => 'Pin Text',
				'selector' => '.pin-board-pin .entry-content',
				'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow')
			));

			$this->register_block_element(array(
				'id' => 'pagination-button',
				'name' => 'Pagination Button',
				'selector' => '.pin-board-pagination a',
				'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
				'states' => array(
					'Hover' => '.pin-board-pagination a:hover',
				)
			));
			$this->register_block_element(array(
				'id' => 'pagination-text',
				'name' => 'Pagination Current Page',
				'selector' => '.pin-board-pagination span.page-numbers.current',
				'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			));

		}


		private static function pinterest_button($url, $image_url) {

			if ( !$url || !$image_url )
				return;

			echo '<a href="http://pinterest.com/pin/create/button/?url=' . rawurlencode($url) . '&media=' . rawurlencode($image_url) . '" class="pin-it-button" count-layout="horizontal"><img border="0" src="" data-src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>';

		}


		private static function twitter_button($url, $title, $username = '', $hashtag = '') {

			if ( !$url )
				return;

			echo '<iframe allowtransparency="true" frameborder="0" scrolling="no" data-src="http://platform.twitter.com/widgets/tweet_button.1340179658.html#_=1343335678535&amp;count=none&amp;hashtags=' . str_replace('#', '', $hashtag) . '&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=' . rawurlencode($url) . '&amp;related=' . $username . '&amp;size=m&amp;text=' . rawurlencode($title) . '&amp;url=' . rawurlencode($url) . '" class="twitter-share-button" title="Twitter Tweet Button"></iframe>';

		}


		private static function facebook_button($url, $verb = 'like') {

			if ( !$url )
				return;

			echo '<iframe class="facebook-share-button facebook-' . $verb . '-button" data-src="//www.facebook.com/plugins/like.php?href=' . rawurlencode($url) . '&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=false&amp;action=' . strtolower($verb) . '&amp;colorscheme=light&amp;font=lucida+grande&amp;height=21" scrolling="no" frameborder="0" allowTransparency="true"></iframe>';

		}


		private static function pagination($query, $infinite_scroll = true,$enumerate) {
			echo '<div class="pin-board-pagination">';
			echo '<span class="nav-next">';
				echo get_next_posts_link('&larr; Older', $query->max_num_pages);
			echo '</span><!-- .next -->';
			if ($enumerate) {
				$big = 999999999; // need an unlikely integer
				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $query->max_num_pages,
					'prev_next' => false
				) );
			}
			echo '<span class="nav-previous">';
				echo self::previous_posts_link('Newer &rarr;');
			echo '</span><!-- .previous -->';
			echo '</div><!-- .pin-board-pagination -->';
		}


			private static function previous_posts_link($label) {

				global $paged;

				if ( null === $label )
					$label = __( '&laquo; Previous Page' );

				if ( $paged > 1 ) {
					$attr = apply_filters( 'previous_posts_link_attributes', '' );
					return '<a href="' . previous_posts( false ) . "\" $attr>". preg_replace( '/&([^#])(?![a-z]{1,8};)/i', '&#038;$1', $label ) .'</a>';
				}

			}


		private static function relative_time($relative = true) {

			if ( $relative ) {

				$post_date = get_the_time('U');
				$delta = time() - $post_date;

				if ( $delta < 60 ) {
				    return 'less than a minute ago';

				} elseif ($delta >= 60 && $delta <= 120){
				    return 'about a minute ago';

				} elseif ($delta >= 120 && $delta <= (60*60)) {
				    return strval(round(($delta/60),0)) . ' minutes ago';

				} elseif ($delta >= (60*60) && $delta <= (120*60)){
				    return 'about an hour ago';

				} elseif ($delta >= (120*60) && $delta <= (24*60*60)){
				    return strval(round(($delta/3600),0)) . ' hours ago';

				}

			}

			return get_the_date('M j, Y');

		}


		public static function excerpt_more($more) {

			return '...';

		}


	}


	class HeadwayPinBoardCoreBlockOptions extends HeadwayBlockOptionsAPI {

		public $taxonomy_list;

		public $terms_list;

		public $tabs = array(
			'pin-setup' => 'Pin Setup',
			'query-filters' => 'Query Filters',
			'text' => 'Text',
			'images' => 'Images',
			'effects' => 'Effects',
			'social' => 'Social'
		);

		// This acgtivates a callback when the block edito0r opens as callbacks in fields are not activated unti8l changed.
		public $open_js_callback = '
			if ( $(\'div#block-\' + blockID + \'-tab\').find(\'select#input-\' + blockID + \'-mode\').val() != \'custom\' )
				$(\'div#block-\' + blockID + \'-tab li#sub-tab-query-filters\').hide();
		';

		public $inputs = array(
			'pin-setup' => array(
				'mode' => array(
					'type' => 'select',
					'name' => 'mode',
					'label' => 'Mode',
					'tooltip' => 'If you would like to modify the default behaviour, select custom query. <br/><strong>Note:</strong>On archive pages, it\'s not advisable to use a custom query if the block is displaying the archive results.<br/>On search pages, it may be necessary to limit results to only certain content types.',
					'options' => array(
						'default' => 'Default behaviour',
						'custom' => 'Custom query',
					),
					'toggle' => array(
						'default' => array(
							'hide' => 'li#sub-tab-query-filters'
						),
						'custom' => array(
							'show' => 'li#sub-tab-query-filters'
						)
					)
				),
				'layout-heading' => array(
					'name' => 'layout-heading',
					'type' => 'heading',
					'label' => 'Layout',
				),
				'columns' => array(
					'type' => 'slider',
					'name' => 'columns',
					'label' => 'Columns',
					'slider-min' => 1,
					'slider-max' => 7,
					'slider-interval' => 1,
					'default' => 3,
					'tooltip' => 'Set how many pins to display horizontally'
				),

				'columns-smartphone' => array(
					'type' => 'slider',
					'name' => 'columns-smartphone',
					'label' => 'Columns (iPhone/Smartphone)',
					'slider-min' => 1,
					'slider-max' => 7,
					'slider-interval' => 1,
					'default' => 2,
					'tooltip' => 'Set how many pins to display horizontally for iPhones and smartphones.  <strong>Recommended setting: 1 or 2</strong>'
				),

				'gutter-width' => array(
					'type' => 'slider',
					'name' => 'gutter-width',
					'label' => 'Gutter Width',
					'slider-min' => 0,
					'slider-max' => 100,
					'slider-interval' => 1,
					'default' => 15,
					'unit' => 'px',
					'tooltip' => 'The amount in space between pins horizontally.'
				),

				'pin-bottom-margin' => array(
					'type' => 'slider',
					'name' => 'pin-bottom-margin',
					'label' => 'Pin Bottom Margin',
					'slider-min' => 0,
					'slider-max' => 50,
					'slider-interval' => 1,
					'default' => 15,
					'unit' => 'px',
					'tooltip' => 'The amount of space on the bottom of each pin.'
				),
				'pagination-heading' => array(
					'name' => 'pagination-heading',
					'type' => 'heading',
					'label' => 'Pagination',
				),
				'pins-per-page' => array(
					'type' => 'integer',
					'name' => 'pins-per-page',
					'label' => 'Pins Per Page',
					'default' => 10,
					'tooltip' => 'Determines how many pins to load at one time before loading more via pagination or <em>infinite scrolling</em>.'
				),

				'paginate' => array(
					'type' => 'checkbox',
					'name' => 'paginate',
					'label' => 'Paginate Pins',
					'default' => true,
					'tooltip' => 'Enabling pagination adds buttons to the bottom of the pin board to go to the next/previous page.  <strong>Note:</strong> If infinite scrolling is enabled, pagination will be hidden.'
				),
				'enumerate' => array(
					'type' => 'checkbox',
					'name' => 'enumerate',
					'label' => 'Enumerate pagination',
					'default' => false,
					'tooltip' => 'If pagination is displayed, enabling this will also show page number navigation.'
				),
				'order-heading' => array(
					'name' => 'order-heading',
					'type' => 'heading',
					'label' => 'Order',
				),
					'order-by' => array(
					'type' => 'select',
					'name' => 'order-by',
					'label' => 'Order By',
					'tooltip' => '',
					'options' => array(
						'date' => 'Date',
						'title' => 'Title',
						'rand' => 'Random',
						'ID' => 'ID'
					)
				),

				'order' => array(
					'type' => 'select',
					'name' => 'order',
					'label' => 'Order',
					'tooltip' => '',
					'options' => array(
						'desc' => 'Descending',
						'asc' => 'Ascending',
					)
				)
			),

			'query-filters' => array(
				'post-type' => array(
					'type' => 'multi-select',
					'name' => 'post-type',
					'label' => 'Post Type',
					'tooltip' => 'Choose a post type to display. If none are selected, it will automatically default to all.',
					'default' => 'any',
					'options' => 'get_post_types()'
				),


				'taxonomies' => array(
					'type' => 'select',
					'name' => 'taxonomies',
					'label' => 'Taxonomy',
					'default' => 'category',
					'options' => array('none' => 'No taxonomies'),
					'tooltip' => 'Select the taxonomy to filter pins on .',
					'callback' => 'reloadBlockOptions()'
				),

				// For simplicity with migrating from categories to all taxonomies, these next two have kept the same names. In the future a function could be written to port them to a correctly named variable
				'categories' => array(
					'type' => 'multi-select',
					'name' => 'categories',
					'label' => 'Terms',
					'default' => '',
					'options' => array('none' => 'No terms'),
					'tooltip' => 'Filter the pins that are shown by the selected taxonomy\'s terms.'
				),

				'categories-mode' => array(
					'type' => 'select',
					'name' => 'categories-mode',
					'label' => 'Terms Mode',
					'tooltip' => '',
					'options' => array(
						'include' => 'Include',
						'exclude' => 'Exclude'
					),
					'tooltip' => 'If this is set to <em>include</em>, then only the pins that match the terms filter will be shown.  If set to <em>exclude</em>, all pins that match the selected terms will not be shown.'
				),

				'author' => array(
					'type' => 'multi-select',
					'name' => 'author',
					'label' => 'Author',
					'tooltip' => '',
					'options' => 'get_authors()'
				),
			),

			'text' => array(
				'show-titles' => array(
					'type' => 'checkbox',
					'name' => 'show-titles',
					'label' => 'Show Titles',
					'default' => true
				),

				'titles-position' => array(
					'type' => 'select',
					'name' => 'titles-position',
					'label' => 'Titles position',
					'default' => 'below',
					'options' => array('above' => 'Above','below' => 'Below')
				),

				'titles-link-to-post' => array(
					'type' => 'checkbox',
					'name' => 'titles-link-to-post',
					'label' => 'Titles link to post',
					'default' => true,
					'tooltip' => 'Open the post when the user clicks on the title'
				),

				'content-to-show' => array(
					'type' => 'select',
					'name' => 'content-to-show',
					'label' => 'Content To Show',
					'options' => array(
						'' => '&ndash; Do Not Show Content &ndash;',
						'excerpt' => 'Excerpts',
						'content' => 'Full Content'
					),
					'default' => 'excerpt',
					'tooltip' => 'The content is the written text or HTML for the entry.  This is edited in the WordPress admin panel.'
				),

				'show-text-if-no-image' => array(
					'type' => 'checkbox',
					'name' => 'show-text-if-no-image',
					'label' => 'Only show content when no featured image',
					'default' => false,
					'tooltip' => 'If enabled, regardless of the content chosen in <em>Content to Show</em> will only show content for pins with no featured image.'
				),

				'show-author' => array(
					'type' => 'checkbox',
					'name' => 'show-author',
					'label' => 'Meta: Show Author "byline"',
					'default' => false,
					'tooltip' => '<strong>Example:</strong> <em>by</em> Author Name'
				),

				'show-datetime' => array(
					'type' => 'checkbox',
					'name' => 'show-datetime',
					'label' => 'Meta: Show Date/Time',
					'default' => false
				),

				'datetime-verb' => array(
					'type' => 'text',
					'name' => 'datetime-verb',
					'label' => 'Meta: Posted Verb',
					'default' => 'Posted',
					'tooltip' => 'The posted verb will be placed before the time.  For instance, you may want to use "Listed" for real estate rather than "Posted"'
				),

				'relative-times' => array(
					'type' => 'checkbox',
					'name' => 'relative-times',
					'label' => 'Meta: Use Relative Times',
					'default' => true,
					'tooltip' => '<strong>Example:</strong> 8 hours ago'
				)
			),

			'images' => array(
				'show-images' => array(
					'type' => 'checkbox',
					'name' => 'show-images',
					'label' => 'Show Images',
					'default' => true,
				),
				'images-click-action' => array(
					'type' => 'select',
					'name' => 'image-click-action',
					'label' => 'Image click action',
					'default' => 'link',
					'tooltip' => 'Choose the action when user clicks on an image.',
					'options' => array(
															'post'  => 'Open post',
															'popup' => 'Popup original image',
															'none'  => 'Do nothing'
														)
				),

				'crop-vertically' => array(
					'type' => 'checkbox',
					'name' => 'crop-vertically',
					'label' => 'Crop Vertically',
					'default' => false,
					'tooltip' => 'Trim all images to have the same height.  The trimmed/cropped height is roughly 75% of the width.'
				)
			),

			'effects' => array(
				'infinite-scroll' => array(
					'type' => 'checkbox',
					'name' => 'infinite-scroll',
					'label' => 'Infinite Scrolling',
					'default' => true,
					'tooltip' => 'Infinite scrolling allows your visitors to view all of your pins without the need for them to click a button to continue to the next page.  The pins will be loaded automatically simply by scrolling. Note: On archives and search result pages, infinite scrolling is disabled as it will not work correctly.'
				),

				'hover-focus' => array(
					'type' => 'checkbox',
					'name' => 'hover-focus',
					'label' => 'Hover Focus',
					'default' => false,
					'tooltip' => 'If enabled, the hovered pin will be focused while all others will be faded out.'
				),
			),

			'social' => array(
				'show-pinterest-button' => array(
					'type' => 'checkbox',
					'name' => 'show-pinterest-button',
					'label' => 'Pinterest: Show "Pin It" Button',
					'default' => false,
					'tooltip' => 'Show a Pinterest "Pin It" button inside of the images.',
				),

				'show-twitter-button' => array(
					'type' => 'checkbox',
					'name' => 'show-twitter-button',
					'label' => 'Twitter: Show Tweet Button',
					'default' => false,
					'tooltip' => 'Show a tweet button either inside of the post image or by the title.',
				),

				'twitter-username' => array(
					'type' => 'text',
					'name' => 'twitter-username',
					'label' => 'Twitter: Your Username'
				),

				'twitter-hashtag' => array(
					'type' => 'text',
					'name' => 'twitter-hashtag',
					'label' => 'Twitter: Hashtag to put in tweets (Optional)'
				),

				'show-facebook-button' => array(
					'type' => 'checkbox',
					'name' => 'show-facebook-button',
					'label' => 'Facebook: Show Like/Share Button',
					'default' => false,
					'tooltip' => 'Show a Facebook share/like button either inside of the post image or by the title.',
				),

				'facebook-button-verb' => array(
					'type' => 'select',
					'label' => 'Facebook: Button Verb',
					'name' => 'facebook-button-verb',
					'options' => array(
						'like' => 'Like',
						'recommend' => 'Recommend'
					),
					'default' => 'like'
				)
			)
		);


		public function modify_arguments($args = false) {
			$block = $args['block'];

			$this->taxonomy_list 	= self::get_taxonomy_list();
			$this->inputs['query-filters']['taxonomies']['options'] = $this->taxonomy_list;

			$tax_slug = HeadwayBlockAPI::get_setting($block, 'taxonomies', 'category');
			$this->terms_list = self::get_tax_terms($tax_slug);

			$this->inputs['query-filters']['categories'] = array(
				'type' => 'multi-select',
				'name' => 'categories',
				'label' => 'Terms',
				'default' => '',
				'options' => $this->terms_list[$tax_slug],
				'tooltip' => 'Filter the pins that are shown by the selected taxonomy\'s terms.'
			);

			$callback = '
					if ( !$("body").hasClass("visual-editor-mode-grid") ) {
						window.frames[\'content\'].setupPinBoardBlock({
							blockID: getBlockID(block),
							effects: {
								hoverFocus: input.parents(".sub-tabs-content-container").find("#input-hover-focus input[type=\'hidden\']").val().toBool(),
								infiniteScroll: input.parents(".sub-tabs-content-container").find("#input-infinite-scroll input[type=\'hidden\']").val().toBool(),
							},
							columns: parseInt(input.parents(".sub-tabs-content-container").find("#input-columns input[type=\'hidden\']").val()),
							columnsSmartphone: parseInt(input.parents(".sub-tabs-content-container").find("#input-columns-smartphone input[type=\'hidden\']").val()),
							gutterWidth: parseInt(input.parents(".sub-tabs-content-container").find("#input-gutter-width input[type=\'hidden\']").val())
						});
					}
				';

			/* Add the callback to all options */
			foreach ( $this->inputs as $tab_id => $inputs )
				foreach ( $this->inputs[$tab_id] as $input_id => $input_options )
					if ( !headway_get('callback', $this->inputs[$tab_id][$input_id]) )
						$this->inputs[$tab_id][$input_id]['callback'] = $callback;

		}


		public static function get_categories() {

			$category_options = array();

			$categories_select_query = get_categories();

			foreach ($categories_select_query as $category)
				$category_options[$category->term_id] = $category->name;

			return $category_options;

		}


		public static function get_authors() {

			$author_options = array();

			$authors = get_users(array(
				'orderby' => 'post_count',
				'order' => 'desc',
				'who' => 'authors'
			));

			foreach ( $authors as $author )
				$author_options[$author->ID] = $author->display_name;

			return $author_options;

		}


		public static function get_post_types() {

			$post_type_options = array();

			$post_types = get_post_types(false, 'objects');

			foreach($post_types as $post_type_id => $post_type){

				//Make sure the post type is not an excluded post type.
				if(in_array($post_type_id, array('revision', 'nav_menu_item')))
					continue;

				$post_type_options[$post_type_id] = $post_type->labels->name;

			}

			return $post_type_options;

		}

		public static function get_taxonomy_list() {

			$custom_tax = get_taxonomies();
			$exclude_list = array('nav_menu','link_category','post_format');
			$tax_array = array();

			foreach ($custom_tax as $tax) {
				if (!in_array($tax, $exclude_list)) {
					$tax_array[$tax] = ucwords(str_replace(array('_','-'), ' ', $tax));
				}
			}

			return $tax_array;

		}

		public static function get_tax_terms($taxonomies, $keys_only = false) {

			if ( !is_array($taxonomies) )
				$taxonomies = array($taxonomies => $taxonomies);

			$terms = array();

			foreach ( $taxonomies as $key => $tax_name ) {

				$term_list = get_terms($key, 'hide_empty=0');

				foreach ($term_list as $term) {

					if ( $keys_only ) {
						$terms[] = $term->slug;
					} else {
						$terms[$key][$term->slug] = $term->name;
					}

				}

				if ( !$keys_only && count($terms[$key]) == 0 ) {
					$terms[$key]['none'] = 'No terms found for this taxonomy';
				}

			}

			return $terms;

		}

	}


	/**
	 * Prevent 404ing from breaking Infinite Scrolling
	 **/
	add_action('status_header', 'hw_pin_board_block_prevent_404');
	function hw_pin_board_block_prevent_404($status) {

		if ( strpos($status, '404') && get_query_var('paged') && headway_get('pb') )
			return 'HTTP/1.1 200 OK';

		return $status;

	}


	/**
	 * Prevent WordPress redirect from messing up pin board pagination
	 */
	add_filter('redirect_canonical', 'hw_pin_board_block_redirect');
	function hw_pin_board_block_redirect($redirect_url) {

		if ( headway_get('pb') )
			return false;

		return $redirect_url;

	}

}
