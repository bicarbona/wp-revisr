<?php
class HeadwayMaintenance {
	
	
	/**
	 * Over time, there may be issues to be corrected between updates or naming conventions to be changed between updates.
	 * All of that will be processed here.
	 **/
	public static function db_upgrade($db_version) {

		global $wpdb;

		ignore_user_abort(true);
		set_time_limit(0);

		/**
		 * Pre-3.6
		 * 
		 * - Merge all design settings into the one option
		 **/
			if ( version_compare($db_version, '3.6', '<') ) {

				HeadwayElementsData::upgrade_to_36_data_scheme();

			}


		/** 
		 * 3.6.1
		 *
		 * Do 3.6 design conversion if the Headway 3.6 design options is the same as the default.  This is to fix the bad 3.6 upgrade bug
		 **/
			if ( version_compare($db_version, '3.6.1', '<') ) {

				if ( HeadwayOption::$current_skin == HEADWAY_DEFAULT_SKIN && HeadwaySkinOption::get('properties', 'design', array()) == HeadwayElementsData::get_default_data() ) {

					$combined_design_settings = array();

					//Fetch all options in wp_options and get old Headway design options
					foreach ( wp_load_alloptions() as $option => $option_value ) {
									
						if ( strpos($option, 'headway_option_group_design-editor-group') !== 0 )
							continue;

						$combined_design_settings = array_merge($combined_design_settings, get_option($option));

					}

					if ( !empty($combined_design_settings) ) {

						HeadwaySkinOption::set('properties', $combined_design_settings, 'design');

						HeadwayElementsData::merge_default_design_data(HeadwayElementsData::get_default_data(), 'core-361-upgrade-fix');
					
					}

				}

			}


        /**
         * 3.7
         *
         * Add new custom MySQL table for blocks and transfer blocks in wp_options over to it
         */
			if ( version_compare($db_version, '3.7', '<') ) {

				Headway::create_mysql_tables();

				/* Set vars */
				$upgraded_blocks = array();
				$upgraded_wrappers = array();

				//<editor-fold desc="Move wrappers to hw_wrappers table">
				$wrappers_by_template = $wpdb->get_results("SELECT * FROM $wpdb->options WHERE option_name LIKE 'headway%option_group_wrappers'");

				foreach ( $wrappers_by_template as $template_wrappers ) {

					if ( strpos($template_wrappers->option_name, 'headway_|skin=') !== 0 ) {
						$template = 'base';
					} else {
						$option_name_fragments = explode('|_', str_replace('headway_|skin=', '', $template_wrappers->option_name));
						$template = $option_name_fragments[0];
					}

					foreach ( maybe_unserialize($template_wrappers->option_value) as $layout_id => $layout_wrappers ) {

						if ( !is_array($layout_wrappers) )
							continue;

						$layout_id = HeadwayLayout::format_old_id($layout_id);

						foreach ( $layout_wrappers as $layout_wrapper_id => $layout_wrapper ) {

							$layout_wrapper['template'] = $template;
							$layout_wrapper['position'] = array_search($layout_wrapper_id, array_keys($layout_wrappers));
							$layout_wrapper['legacy_id'] = HeadwayWrappers::format_wrapper_id($layout_wrapper_id);

							$layout_wrapper['settings'] = array(
								'fluid'                => headway_get('fluid', $layout_wrapper),
								'fluid-grid'           => headway_get('fluid-grid', $layout_wrapper),
								'columns'              => headway_get('columns', $layout_wrapper),
								'column-width'         => headway_get('column-width', $layout_wrapper),
								'gutter-width'         => headway_get('gutter-width', $layout_wrapper),
								'use-independent-grid' => headway_get('use-independent-grid', $layout_wrapper)
							);

							$new_wrapper = HeadwayWrappersData::add_wrapper($layout_id, $layout_wrapper);

							if ( $new_wrapper && !is_wp_error($new_wrapper) ) {

								if ( !isset( $upgraded_wrappers[ $template ]) )
									$upgraded_wrappers[ $template ] = array();

								$upgraded_wrappers[$template][HeadwayWrappers::format_wrapper_id($layout_wrapper_id)] = array(
									'id'             => $new_wrapper,
									'mirror-wrapper' => headway_get('mirror-wrapper', $layout_wrapper)
								);

							}

						}

					}

				}
				//</editor-fold>

				//<editor-fold desc="Move layout specific options and blocks">
				$all_layout_options = self::extract_layout_options();

				foreach ( $all_layout_options as $template => $template_layouts ) {

					foreach ( $template_layouts as $template_layout_id => $template_layout_options ) {

						/* Move blocks to hw_blocks table */
							foreach ( headway_get('blocks', headway_get('general', $template_layout_options), array()) as $block_id => $block ) {

								$block['template'] = $template;

								$wrapper = headway_get( HeadwayWrappers::format_wrapper_id( $block['wrapper'] ), headway_get($template, $upgraded_wrappers, array()) );

								$block['wrapper'] = $wrapper['id'];
								$block['legacy_id'] = $block_id;

								$new_block = HeadwayBlocksData::add_block( HeadwayLayout::format_old_id( $template_layout_id ), $block );

								if ( $new_block && !is_wp_error($new_block) ) {

									if ( ! isset( $upgraded_blocks[ $template ] ) )
										$upgraded_blocks[ $template ] = array();

									$upgraded_blocks[ $template ][ $block_id ] = array(
										'id'           => $new_block,
										'mirror-block' => headway_get( 'mirror-block', headway_get( 'settings', $block ) )
									);

								}

							}

						/* Move layout meta from postmeta capable layouts to the wp_postmeta table */
							foreach ( $template_layout_options as $template_layout_options_group => $template_layout_options_group_options ) {

								foreach ( $template_layout_options_group_options as $option => $option_value ) {

									if ( in_array($option, array('customized', 'blocks')) )
										continue;

									$global = $option == 'template' ? false : true;

									HeadwayLayoutOption::set( HeadwayLayout::format_old_id( $template_layout_id ), $option, $option_value, $global, $template_layout_options_group );


								}

							}


					}


				}
				//</editor-fold>


				//<editor-fold desc="Setup mirroring">
				foreach ( $upgraded_blocks as $template => $template_blocks ) {

					foreach ( $template_blocks as $old_block_id => $new_block_info ) {

						if ( ! $mirror_block = headway_get( headway_get( 'mirror-block', $new_block_info ), $template_blocks ) )
							continue;

						$mirror_id = headway_get( 'id', $mirror_block );

						HeadwayBlocksData::update_block( $new_block_info['id'], array(
							'mirror_id' => $mirror_id,
							'template'  => $template
						) );

					}

				}


				foreach ( $upgraded_wrappers as $template => $template_wrappers ) {

					foreach ( $template_wrappers as $old_wrapper_id => $new_wrapper_info ) {

						if ( ! $mirror_wrapper = headway_get( HeadwayWrappers::format_wrapper_id(headway_get( 'mirror-wrapper', $new_wrapper_info )), $template_wrappers ) )
							continue;

						$mirror_id = headway_get( 'id', $mirror_wrapper );

						HeadwayWrappersData::update_wrapper( $new_wrapper_info['id'], array(
							'mirror_id' => $mirror_id,
							'template'  => $template
						) );

					}

				}
				//</editor-fold>


				//<editor-fold desc="Copy/rename Base template option groups">
				add_option( 'headway_|skin=base|_option_group_design', get_option( 'headway_option_group_design', array() ) );
				add_option( 'headway_|skin=base|_option_group_general', get_option( 'headway_option_group_general', array() ) );
				add_option( 'headway_|skin=base|_option_group_templates', get_option( 'headway_option_group_templates', array() ) );

				wp_cache_flush();

				/* Pull settings from headway_option_group_general and headway_|skin=base|_option_group_general */
				$option_group_general = get_option('headway_option_group_general');

				foreach ( $option_group_general as $option => $option_value ) {

					$options_to_remove = array(
						'cache',
						'colorpicker-swatches',
						'webfont-cache'
					);

					if ( in_array($option, $options_to_remove) || strpos( $option, 'merged-default-design-data-' ) === 0 ) {
						unset( $option_group_general[ $option ] );
					}

				}

				update_option('headway_option_group_general', $option_group_general);
				//</editor-fold>


				//<editor-fold desc="Link new block/wrapper IDs to design settings">
				/* Sort the block and wrapper mappings by descending number that way when we do a simple recursive find and replace the small block IDs won't mess up the larger block IDs.
				Example: Replacing block-1 before block-11 is replaced would be bad news */
				foreach ( $upgraded_blocks as $template => $template_blocks ) {

					$template_design_settings = get_option( 'headway_|skin=' . $template . '|_option_group_design', array() );

					krsort( $template_blocks );

					foreach ( $template_blocks as $old_block_id => $new_block_info ) {

						$template_design_settings = headway_str_replace_json( 'block-' . $old_block_id, 'block-' . $new_block_info['id'], $template_design_settings );

					}

					update_option( 'headway_|skin=' . $template . '|_option_group_design', $template_design_settings );

				}


				foreach ( $upgraded_wrappers as $template => $template_wrappers ) {

					$template_design_settings = get_option( 'headway_|skin=' . $template . '|_option_group_design', array() );

					krsort( $template_wrappers );

					foreach ( $template_wrappers as $old_wrapper_id => $new_wrapper_info ) {

						$template_design_settings = headway_str_replace_json( 'wrapper-' . $old_wrapper_id, 'wrapper-' . $new_wrapper_info['id'], $template_design_settings );

					}

					update_option( 'headway_|skin=' . $template . '|_option_group_design', $template_design_settings );

				}
				//</editor-fold>


				//<editor-fold desc="Loop through Headway options and rename/delete/modify them if necessary">
				$wpdb->query( "UPDATE $wpdb->options SET option_name = replace(option_name, 'headway_|skin=', 'headway_|template=')" );

				$wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE 'headway_%layout_options_%'" );


				$wpdb->delete( $wpdb->options, array(
					'option_name' => 'headway_option_group_design'
				) );

				$wpdb->delete( $wpdb->options, array(
					'option_name' => 'headway_option_group_wrappers'
				) );

				$wpdb->delete( $wpdb->options, array(
					'option_name' => 'headway_option_group_templates'
				) );

				$wpdb->delete( $wpdb->options, array(
					'option_name' => 'headway_option_group_blocks'
				) );

				$wpdb->delete( $wpdb->options, array(
					'option_name' => 'headway_option_group_block-actions-cache'
				) );
				//</editor-fold>


				//<editor-fold desc="Add Pin Board styling for each template">
				foreach ( $upgraded_wrappers as $template => $template_wrappers ) {

					HeadwayOption::$current_skin = $template;

					HeadwayElementsData::merge_default_design_data( array(
						'block-pin-board-pin'               => array(
							'properties' => array(
								'padding-top'                  => 1,
								'padding-right'                => 1,
								'padding-bottom'               => 1,
								'padding-left'                 => 1,
								'background-color'             => 'ffffff',
								'border-color'                 => 'eeeeee',
								'border-style'                 => 'solid',
								'border-top-width'             => 1,
								'border-right-width'           => 1,
								'border-bottom-width'          => 1,
								'border-left-width'            => 1,
								'box-shadow-color'             => 'eee',
								'box-shadow-blur'              => 3,
								'box-shadow-horizontal-offset' => 0,
								'box-shadow-vertical-offset'   => 2
							)
						),
						'block-pin-board-pin-title'         => array(
							'properties'            => array(
								'padding-top'     => 15,
								'padding-right'   => 15,
								'padding-left'    => 15,
								'font-size'       => 18,
								'line-height'     => 120,
								'text-decoration' => 'none'
							),
							'special-element-state' => array(
								'hover' => array(
									'text-decoration' => 'underline'
								)
							)
						),
						'block-pin-board-pin-text'          => array(
							'properties' => array(
								'font-size'     => 12,
								'line-height'   => 150,
								'padding-right' => 15,
								'padding-left'  => 15
							)
						),
						'block-pin-board-pin-meta'          => array(
							'properties' => array(
								'font-size'     => 12,
								'line-height'   => 120,
								'padding-right' => 15,
								'padding-left'  => 15,
								'color'         => '888888'
							)
						),
						'block-pin-board-pagination-button' => array(
							'properties'            => array(
								'text-decoration'            => 'none',
								'background-color'           => 'eeeeee',
								'border-top-left-radius'     => 4,
								'border-top-right-radius'    => 4,
								'border-bottom-right-radius' => 4,
								'border-bottom-left-radius'  => 4,
								'padding-top'                => 5,
								'padding-right'              => 9,
								'padding-bottom'             => 5,
								'padding-left'               => 9
							),
							'special-element-state' => array(
								'hover' => array(
									'background-color' => 'e7e7e7'
								)
							)
						)
					), 'core-37-pin-board' );

				}

				/* Set skin back */
				HeadwayOption::$current_skin = HeadwayTemplates::get_active_id();
				//</editor-fold>


				//<editor-fold desc="Turn autoload off for Headway options then back on for the current template options as well as the headway_option_group_general option">
				$wpdb->query("UPDATE $wpdb->options SET autoload = 'no' WHERE option_name LIKE 'headway_%'");

				$wpdb->update($wpdb->options, array(
					'autoload' => 'yes'
				), array(
					'option_name' => 'headway_option_group_general'
				));

				$wpdb->query("UPDATE $wpdb->options SET autoload = 'yes' WHERE option_name LIKE 'headway_|template=" . HeadwayOption::$current_skin . "|%'");
				//</editor-fold>


			}


        /* Add action to flush caches */
        do_action('headway_db_upgrade');
		
		/* Update the version here. */
		$headway_settings = get_option('headway', array('version' => 0));
		$headway_settings['version'] = HEADWAY_VERSION;

		update_option('headway', $headway_settings);
		
		return true;
		
	}


	public static function extract_layout_options() {

		global $wpdb;

		$all_layout_options = array();

		/* Build layout options catalog */
			foreach ( $wpdb->get_results( "SELECT option_name, option_value FROM $wpdb->options WHERE option_name LIKE 'headway_%'" ) as $option_obj ) {

				$option = $option_obj->option_name;
				$option_value = $option_obj->option_value;


				if ( $option == 'headway_layout_options_catalog' || (strpos($option, 'headway_') === 0 && substr($option, -8) == '_preview') )
					continue;

				if ( strpos($option, 'headway_') !== 0 || strpos($option, 'layout_options') === false )
					continue;

				/* Figure out template ID and layout */
					if ( strpos($option, 'headway_|skin=') !== 0 ) {

						$template = 'base';
						$layout = str_replace('headway_layout_options_', '', $option);

					} else {

						$option_name_fragments = explode('|_', str_replace('headway_|skin=', '', $option));

						$template = $option_name_fragments[0];
						$layout = str_replace('layout_options_', '', $option_name_fragments[1]);

					}

					/* If the layout ID is template then change the underscore to a hyphen */
					if ( strpos($layout, 'template_') === 0 )
						$layout = str_replace('template_', 'template-', $layout);

				/* If the layout is numeric, then check if the post even exists and isn't a revision.  If it does not exist or is a revision, delete it! */
					if ( is_numeric($layout) ) {

						$post_row = $wpdb->get_row($wpdb->prepare("SELECT * FROM $wpdb->posts WHERE ID = %d LIMIT 1", $layout));

						if ( $post_row )
							$post_status = get_post_status($layout);

						/* If the post row is false (doesn't exist) or post status is revision (AKA inherit) then delete the whole layout option group */
						if ( !$post_row || $post_status == 'inherit' ) {
							delete_option($option);
							continue;
						}

					}

				/* Add to return array */
					if ( !isset($all_layout_options[$template]) )
						$all_layout_options[$template] = array();

					$all_layout_options[$template][$layout] = maybe_unserialize($option_value);

			}

		return $all_layout_options;

	}

	
}