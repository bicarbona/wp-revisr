<?php

define("MY_THEME_NAME", "Kinorama 2013");
define("MY_THEME_VERSION", " 0.1");

/**
 Put Separators Back In ADMIN
**/ 
add_action('admin_head', 'admin_separator');
 
function admin_separator() {
  echo '<style>
  #adminmenu li.wp-menu-separator {
	background-color: rgba(0,0,0,0.06);
  }
  
 body,
  input,
  textarea,
  submit,
  .press-this a.wp-switch-editor {
 
  }
  </style>';
}


/**
 SANITIZE FILENAME  -  odstrani diakritku
**/
add_filter( 'sanitize_file_name', 'extended_sanitize_file_name', 10, 2 );
function extended_sanitize_file_name( $filename ) {
	$sanitized_filename = remove_accents( $filename );
	return $sanitized_filename;
}


/**
Disable WordPress Admin Bar for all users but admins
**/

if (!current_user_can('administrator')):
  show_admin_bar(false);
endif;

	// add a favicon for your admin
	function admin_favicon() {
		echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('template_directory').'/images/icons/favicon.ico" />';
	}
	add_action('admin_head', 'admin_favicon');

/**
Farebne oznacenie clankov podla statusu
**/
	add_action('admin_footer','posts_status_color');
	function posts_status_color(){
	?>
	<style>
	.status-draft{background: #FCE3F2 !important;}
	.status-pending{background: #d3e9a0 !important;}
	.status-publish{/* no background keep wp alternating colors */}
	.status-future{background: #C6EBF5 !important;}
	.status-private{background:#F2D46F;}
	</style>
	<?php
	}


/**
Unregister all default WP Widgets
**/

function unregister_default_wp_widgets() {
	//unregister_widget('WP_Widget_Pages');
	//unregister_widget('WP_Widget_Calendar');
	unregister_widget('WP_Widget_Archives');
	unregister_widget('WP_Widget_Links');
	unregister_widget('WP_Widget_Meta');
//	unregister_widget('WP_Widget_Search');
	unregister_widget('WP_Widget_Text');
//	unregister_widget('WP_Widget_Categories');
	unregister_widget('WP_Widget_Recent_Posts');
	unregister_widget('WP_Widget_Recent_Comments');
	unregister_widget('WP_Widget_RSS');
	unregister_widget('WP_Widget_Tag_Cloud');
	unregister_widget('Woo_Widget_Twitter');
	unregister_widget("Woo_Widget_AdSpace");
}
add_action('widgets_init', 'unregister_default_wp_widgets', 1);


// Custom login

	function custom_login() { 
	$theme_path = get_bloginfo('stylesheet_directory');
	echo '<link rel="stylesheet" type="text/css" href="'.$theme_path.'/login.css" />'; 
	}
	add_action('login_head', 'custom_login');

// Login page link redirect to my page
	function my_custom_login_url() {
	  return site_url();
	}
	add_filter( 'login_headerurl', 'my_custom_login_url', 10, 4 );

	function collage_custom_login_title($message)
	{
	// Return title text for the logo to replace 'wordpress'; in this case, the blog name.
	return get_bloginfo('name');
	}
	add_filter("login_headertitle","collage_custom_login_title");


// customize admin footer text

function remove_footer_admin () {
wp_loginout( $redirect );
}
add_filter('admin_footer_text', 'remove_footer_admin');

function change_footer_version() {
  return 'credit <a href="http://www.etienne.sk" target="_blank">Marko Etienne</a> | ' . MY_THEME_NAME. MY_THEME_VERSION;
}
add_filter( 'update_footer', 'change_footer_version', 9999 );


//Customize the Dashboard

	add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

	function my_custom_dashboard_widgets() {
	   global $wp_meta_boxes;
	
	//Remove these dashboard widgets...
	unset($wp_meta_boxes['dashboard']['normal']['high']['dashboard_browser_nag']); //Browse Happy
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	//Add a custom widget called 'Help and Support'
	 //  wp_add_dashboard_widget('custom_help_widget', 'Help and Support', 'custom_dashboard_help');
	}
	//  This is the content for your custom widget
	 function custom_dashboard_help() {
	    echo '<p>Lorum ipsum delor sit amet et nunc</p>';
	}
	
	if (!current_user_can('manage_options')) {
		add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
	}

	

		// REMOVE MENU ADMIN
				function remove_menus () {
				global $menu;
					$restricted = array(  __('Links') , __('Comments')); //  __('Media'),  __('Comments'), __('Posts'),__('Dashboard'),
					end ($menu);
					while (prev($menu)){
						$value = explode(' ',$menu[key($menu)][0]);
						if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
					}
				}
				add_action('admin_menu', 'remove_menus');
?>